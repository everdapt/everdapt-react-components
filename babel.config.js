module.exports = {
  env: {
    test: {
      presets: [['@babel/preset-env', { modules: 'cjs', debug: false }], '@babel/preset-react'],
      plugins: ['@babel/plugin-syntax-dynamic-import', '@babel/plugin-proposal-class-properties'],
    },
    build: {
      presets: [['@babel/preset-env', { modules: false }], '@babel/preset-react'],
      plugins: ['@babel/plugin-syntax-dynamic-import', '@babel/plugin-proposal-class-properties'],
    },
  },
};
