import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Button from './Button';

const btnText = 'Button';
const typeSubmit = 'submit';
const typeDefault = 'button';

describe('Button', () => {
  it('renders correctly', () => {
    const ButtonComponent = renderer.create(<Button>{btnText}</Button>).toJSON();
    expect(ButtonComponent).toMatchSnapshot();
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      isDisabled: true,
    };
    const ButtonComponent = mount(<Button {...props}>{btnText}</Button>)
      .find('.btn')
      .filterWhere(it => it.prop('disabled') === true);
    expect(ButtonComponent).toHaveLength(1);
    expect(ButtonComponent.getDOMNode().getAttribute('disabled')).toBeDefined();
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      isDisabled: false,
    };
    const ButtonComponent = mount(<Button {...props}>{btnText}</Button>)
      .find('.btn')
      .filterWhere(it => it.prop('disabled') === false);
    expect(ButtonComponent).toHaveLength(1);
    expect(ButtonComponent.getDOMNode().getAttribute('disabled')).toBeNull();
  });

  it('should use provided props', () => {
    const props = {
      btnType: 'submit',
      isDisabled: false,
      className: 'button',
    };
    const ButtonComponent = mount(<Button {...props}>{btnText}</Button>);
    expect(ButtonComponent.props().btnType).toEqual(typeSubmit);
    expect(ButtonComponent.props().isDisabled).toEqual(false);
    expect(ButtonComponent.props().className).toEqual('button');
  });

  it('should default to type=button', () => {
    const ButtonComponent = mount(<Button>{btnText}</Button>);
    expect(ButtonComponent.props().btnType).toEqual(typeDefault);
  });

  it('should show the correct text', () => {
    const ButtonComponent = mount(<Button>{btnText}</Button>);
    expect(ButtonComponent.text()).toEqual(btnText);
  });

  // TODO: Add onClick fn test
});
