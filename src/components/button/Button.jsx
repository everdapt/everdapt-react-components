import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

/* eslint-disable react/button-has-type */
class Button extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
    btnType: PropTypes.string,
    isDisabled: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    onClick: null,
    isDisabled: false,
    btnType: 'button',
    className: 'btn',
  };

  render() {
    const { onClick, className, children, btnType, isDisabled } = this.props;
    return (
      <button className={className} onClick={onClick} type={btnType} disabled={isDisabled}>
        {children}
      </button>
    );
  }
}
/* eslint-enable */

export default Button;
