import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import SubmitButton from './SubmitButton';

const btnText = 'Submit';
const btnType = 'submit';

describe('Button', () => {
  it('renders correctly', () => {
    const props = {
      btnText,
    };
    const SubmitButtonComponent = renderer.create(<SubmitButton {...props} />).toJSON();
    expect(SubmitButtonComponent).toMatchSnapshot();
  });

  it('should use defaults', () => {
    const props = {
      btnText,
    };
    const SubmitButtonComponent = mount(<SubmitButton {...props} />);
    expect(SubmitButtonComponent.props().isDisabled).toEqual(false);
    expect(SubmitButtonComponent.props().className).toEqual('form-element form-element--submit');
    expect(SubmitButtonComponent.props().btnClassName).toEqual('btn btn-action');
  });

  it('should use provided props', () => {
    const props = {
      btnText,
      isDisabled: true,
      className: 'xxx',
      btnClassName: 'yyy',
    };
    const SubmitButtonComponent = mount(<SubmitButton {...props} />);
    expect(SubmitButtonComponent.props().isDisabled).toEqual(true);
    expect(SubmitButtonComponent.props().className).toEqual('xxx');
    expect(SubmitButtonComponent.props().btnClassName).toEqual('yyy');
    expect(SubmitButtonComponent.find('Button').text()).toEqual(btnText);
  });

  it('should have the correct type', () => {
    const props = {
      btnText,
    };
    const SubmitButtonComponent = mount(<SubmitButton {...props} />);
    expect(
      SubmitButtonComponent.find('Button')
        .find('button')
        .getDOMNode()
        .getAttribute('type'),
    ).toEqual(btnType);
  });
});
