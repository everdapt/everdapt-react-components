import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Modal from './Modal';

const modalTitle = 'My modal title';

const props = {
  isShowing: false,
  modalTitle,
};

describe('Modal', () => {
  it('renders correctly', () => {
    const ModalComponent = renderer.create(<Modal {...props}>Modal body</Modal>).toJSON();
    expect(ModalComponent).toMatchSnapshot();
  });

  it('when modal is showing add .modal--open class to modal root div', () => {
    const defaultProps = {
      ...props,
      isShowing: true,
    };
    const ModalComponent = mount(<Modal {...defaultProps}>Modal body</Modal>).find('.modal');
    expect(ModalComponent.hasClass('modal--open')).toEqual(true);
  });

  it('when modal is showing, add .has-modal class to document.body', () => {
    const defaultProps = {
      ...props,
      isShowing: true,
    };
    mount(<Modal {...defaultProps}>Modal body</Modal>).find('.modal');
    expect(document.body.classList.contains('has-modal')).toEqual(true);
  });

  it('when modal is showing, should call optionally provided onOpen function', () => {
    const onOpen = () => {
      document.body.classList.add('opened-modal');
    };

    const defaultProps = {
      ...props,
      onOpen,
      isShowing: true,
    };
    mount(<Modal {...defaultProps}>Modal body</Modal>).find('.modal');
    expect(document.body.classList.contains('opened-modal')).toEqual(true);
  });

  it('when modal is closed should call optionally provided onClose function', () => {
    const onClose = () => {
      document.body.classList.add('closed-modal');
    };

    const defaultProps = {
      ...props,
      onClose,
      isShowing: false,
    };
    mount(<Modal {...defaultProps}>Modal body</Modal>).find('.modal');
    expect(document.body.classList.contains('closed-modal')).toEqual(true);
  });
});
