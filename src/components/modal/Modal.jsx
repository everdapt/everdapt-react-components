import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Modal extends Component {
  static propTypes = {
    isShowing: PropTypes.bool.isRequired,
    modalTitle: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
  };

  static defaultProps = {
    onOpen: () => {},
    onClose: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isShowing: false,
    };
  }

  componentDidMount() {
    const { isShowing, onOpen, onClose } = this.props;
    isShowing ? onOpen() : onClose();
    this.setState({
      isShowing,
    });
  }

  closeModal = () => {
    this.setState({
      isShowing: false,
    });
  };

  render() {
    const { modalTitle, children } = this.props;
    const { isShowing } = this.state;
    const classNames = ['modal'];

    if (isShowing) {
      classNames.push('modal--open');
      document.body.classList.add('has-modal');
    } else {
      document.body.classList.remove('has-modal');
    }

    return (
      <div className={`${classNames.join(' ')}`}>
        <div className="modal-content">
          <button className="modal-close-icon modal-close" type="button" onClick={this.closeModal} />
          <div className="modal-inner">
            <div className="modal-header">
              <h2>{modalTitle}</h2>
            </div>
            {children}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
