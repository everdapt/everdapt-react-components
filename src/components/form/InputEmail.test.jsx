import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputEmail from './InputEmail';

const id = 'InputEmail';
const labelText = 'My Email Input';
const inputType = 'email';

describe('InputEmail', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const InputEmailComponent = renderer.create(<InputEmail {...props} />).toJSON();
    expect(InputEmailComponent).toMatchSnapshot();
  });

  it('has the correct type', () => {
    const props = {
      id,
      labelText,
    };
    const InputEmailComponent = mount(<InputEmail {...props} />);
    expect(
      InputEmailComponent.find('InputField')
        .find('input')
        .getDOMNode()
        .getAttribute('type'),
    ).toEqual(inputType);
  });
});
