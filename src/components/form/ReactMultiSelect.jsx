import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

const styles = {
  clearIndicator: () => ({}),
  dropdownIndicator: () => ({}),
  container: () => ({}),
  control: () => ({}),
  group: () => ({}),
  groupHeading: () => ({}),
  indicatorsContainer: () => ({}),
  indicatorSeparator: () => ({}),
  input: () => ({}),
  loadingIndicator: () => ({}),
  loadingMessage: () => ({}),
  menu: () => ({}),
  menuList: () => ({}),
  menuPortal: () => ({}),
  multiValue: () => ({}),
  multiValueLabel: () => ({}),
  multiValueRemove: () => ({}),
  noOptionsMessage: () => ({}),
  option: () => ({}),
  placeholder: () => ({}),
  singleValue: () => ({}),
  valueContainer: () => ({}),
};

class ReactMultiSelect extends PureComponent {
  /* eslint-disable react/forbid-prop-types */
  static propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array,
    value: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    minItems: PropTypes.number,
    maxItems: PropTypes.number,
    labelText: PropTypes.string,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    hintText: PropTypes.string,
    errorText: PropTypes.string,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isSearchable: PropTypes.bool,
    isClearable: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onChange: PropTypes.func,
  };
  /* eslint-enable */

  static defaultProps = {
    options: null,
    value: null,
    minItems: 0,
    maxItems: 0,
    labelText: null,
    className: null,
    placeholder: null,
    hintText: null,
    errorText: null,
    isRequired: false,
    isDisabled: false,
    isSearchable: true,
    isClearable: true,
    autoFocus: false,
    onChange: null,
  };

  render() {
    const {
      name,
      options,
      value,
      minItems,
      maxItems,
      labelText,
      placeholder,
      hintText,
      errorText,
      isRequired,
      isDisabled,
      isSearchable,
      isClearable,
      autoFocus,
      onChange,
    } = this.props;

    let required = isRequired;
    let disabled = isDisabled;
    const classNames = ['form-element', 'form-element--multiselect'];
    let currentItems = 0;
    if (value) {
      if (Array.isArray(value)) {
        currentItems = value.length;
      } else {
        currentItems = Object.keys(value).length ? 1 : 0;
      }
    }
    if (currentItems === maxItems) disabled = true;
    if (minItems) required = true;

    if (errorText) classNames.push('form-element--error');
    if (required) classNames.push('form-element--required');

    const multiProps = {
      name,
      options,
      value,
      classNamePrefix: 'react-select',
      className: 'input--react-select--multi',
      placeholder,
      isDisabled: disabled,
      isSearchable,
      isClearable,
      isMulti: true,
      autoFocus,
      onChange,
      styles,
    };
    /* eslint-disable jsx-a11y/label-has-for */
    return (
      <div className={classNames.join(' ')}>
        {labelText && (
          <label className="label" htmlFor={name} id={`${name}-label`}>
            {labelText}
            {isRequired && <span className="required">*</span>}
          </label>
        )}
        {hintText && (
          <span id={`${name}-hint`} className="form-element-hint">
            {hintText}
          </span>
        )}
        {errorText && <span className="form-element-error-msg">{errorText}</span>}
        <Select {...multiProps} />
      </div>
    );
    /* eslint-enable */
  }
}

export default ReactMultiSelect;
