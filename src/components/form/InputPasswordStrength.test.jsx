import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import ReactTestUtils from 'react-dom/test-utils';
import InputPasswordStrength from './InputPasswordStrength';

const id = 'InputPasswordStrength';
const labelText = 'My Search Input';

describe('InputPasswordStrength', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const InputPasswordStrengthComponent = renderer.create(<InputPasswordStrength {...props} />).toJSON();
    expect(InputPasswordStrengthComponent).toMatchSnapshot();
  });

  it('should shows password invalid if short password provided', () => {
    const props = {
      id,
      labelText,
      minScore: 2,
    };
    const InputPasswordStrengthComponent = mount(<InputPasswordStrength {...props} />);
    const node = InputPasswordStrengthComponent.find('input').getDOMNode();
    ReactTestUtils.Simulate.change(node, {
      target: {
        checked: true,
        value: 'aa',
      },
    });
    expect(InputPasswordStrengthComponent.state().isValid).toEqual(false);
  });

  it('should shows too short description if less than minLength password provided', () => {
    const props = {
      id,
      labelText,
      minLength: 5,
    };
    const strengthState = 'Too short';
    const InputPasswordStrengthComponent = mount(<InputPasswordStrength {...props} />);
    const node = InputPasswordStrengthComponent.find('input').getDOMNode();
    ReactTestUtils.Simulate.change(node, {
      target: {
        checked: true,
        value: 'aaaa',
      },
    });
    const expectedStrengthState = mount(<InputPasswordStrength {...props} />)
      .find('.form-element')
      .find('.password-strength-state')
      .text();
    expect(expectedStrengthState).toEqual(strengthState);
  });
});
