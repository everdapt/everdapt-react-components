import React from 'react';
import renderer from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';
import { mount } from 'enzyme';
import InputPassword from './InputPassword';

const id = 'InputPassword';
const labelText = 'My Search Input';

describe('InputPassword', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const InputPasswordComponent = renderer.create(<InputPassword {...props} />).toJSON();
    expect(InputPasswordComponent).toMatchSnapshot();
  });

  it('should be required if isRequired = true', () => {
    const props = {
      id,
      labelText,
      isRequired: true,
    };
    const isRequired = mount(<InputPassword {...props} />)
      .find('.form-element')
      .hasClass('form-element--required');
    expect(isRequired).toEqual(true);
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      id,
      labelText,
    };
    const isRequired = mount(<InputPassword {...props} />)
      .find('.form-element')
      .hasClass('form-element--required');
    expect(isRequired).toEqual(false);
  });

  it('should not be required if isRequired = false', () => {
    const props = {
      id,
      labelText,
      isRequired: false,
    };
    const isRequired = mount(<InputPassword {...props} />)
      .find('.form-element')
      .hasClass('form-element--required');
    expect(isRequired).toEqual(false);
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      labelText,
      isDisabled: true,
    };
    const isDisabled = mount(<InputPassword {...props} />)
      .find('input')
      .props().disabled;
    expect(isDisabled).toEqual(true);
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      id,
      labelText,
    };
    const isDisabled = mount(<InputPassword {...props} />)
      .find('input')
      .props().disabled;
    expect(isDisabled).toEqual(false);
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      labelText,
      isDisabled: false,
    };
    const isDisabled = mount(<InputPassword {...props} />)
      .find('input')
      .props().disabled;
    expect(isDisabled).toEqual(false);
  });

  it('should shows password strength when shouldCheckStrength is true', () => {
    const props = {
      id,
      labelText,
      shouldCheckStrength: true,
    };
    const shouldCheckStrength = mount(<InputPassword {...props} />)
      .find('.form-element')
      .find('span')
      .hasClass('password-strength-state');
    expect(shouldCheckStrength).toEqual(true);
  });

  it('should shows password strength status when shouldCheckStrength is true and strengthState is provided', () => {
    const strengthState = 'Too short';
    const props = {
      id,
      labelText,
      shouldCheckStrength: true,
      strengthState,
    };
    const expectedStrengthState = mount(<InputPassword {...props} />)
      .find('.form-element')
      .find('.password-strength-state')
      .text();
    expect(expectedStrengthState).toEqual(strengthState);
  });

  it('should shows password text when clicking on show button', () => {
    const props = {
      id,
      labelText,
    };
    const InputPasswordComponent = mount(<InputPassword {...props} />);
    const button = InputPasswordComponent.find('.show-password').getDOMNode();
    ReactTestUtils.Simulate.click(button);
    expect(InputPasswordComponent.state().isShowingPassword).toEqual(true);
  });

  it('should hide password text when clicking on hide button', () => {
    const props = {
      id,
      labelText,
    };
    const InputPasswordComponent = mount(<InputPassword {...props} />);
    const button = InputPasswordComponent.find('.show-password').getDOMNode();
    ReactTestUtils.Simulate.click(button);
    ReactTestUtils.Simulate.click(button);
    expect(InputPasswordComponent.state().isShowingPassword).toEqual(false);
  });

  it('should shows password text when showByDefault is true', () => {
    const props = {
      id,
      labelText,
      showByDefault: true,
    };
    const InputPasswordComponent = mount(<InputPassword {...props} />);
    expect(InputPasswordComponent.find('input').props().type).toEqual('text');
  });
});
