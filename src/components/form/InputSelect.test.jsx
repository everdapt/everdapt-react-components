import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputSelect from './InputSelect';

const id = 'InputSelect';
const labelText = 'My Select Field';
const props = {
  id,
  currentValue: '1',
  options: [{ label: 'One', value: '1' }, { label: 'Two', value: '2' }, { label: 'Three', value: '3' }],
  labelText,
  onChange: () => {},
};

describe('InputSelect', () => {
  it('renders correctly', () => {
    const defaultProps = {
      ...props,
      onChange: null,
    };
    const InputSelectComponent = renderer.create(<InputSelect {...defaultProps} />).toJSON();
    expect(InputSelectComponent).toMatchSnapshot();
  });

  it('has the correct number of options', () => {
    const InputSelectComponent = mount(<InputSelect {...props} />);
    expect(InputSelectComponent.find('option')).toHaveLength(3);
  });

  it('the correct option is selected by default', () => {
    const InputSelectNode = mount(<InputSelect {...props} />)
      .find('select')
      .getDOMNode();
    const currentIndex = InputSelectNode.selectedIndex;
    const currentSelected = InputSelectNode.options[currentIndex].value;
    expect(currentSelected).toEqual('1');
  });

  it('isButton adds the css class', () => {
    const btnProps = {
      ...props,
      isButton: true,
    };
    const InputSelectComponent = mount(<InputSelect {...btnProps} />).find('.form-element--select');
    expect(InputSelectComponent.hasClass('button')).toEqual(true);
  });

  // TODO: test onChange
});
