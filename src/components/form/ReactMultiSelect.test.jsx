import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import ReactMultiSelect from './ReactMultiSelect';

const name = 'React Multi Select';
const labelText = 'My Multi Select';

const props = {
  name,
  value: [{ label: 'One', value: '1' }, { label: 'Three', value: '3' }],
  options: [{ label: 'One', value: '1' }, { label: 'Two', value: '2' }, { label: 'Three', value: '3' }],
  labelText,
  onChange: () => {},
};

describe('ReactMultiSelect', () => {
  it('renders correctly', () => {
    const defaultProps = {
      ...props,
      onChange: null,
    };
    const MultiSelect = renderer.create(<ReactMultiSelect {...defaultProps} />).toJSON();
    expect(MultiSelect).toMatchSnapshot();
  });

  it('the correct options are selected by default', () => {
    const MultiSelect = mount(<ReactMultiSelect {...props} />);
    expect(MultiSelect.find('MultiValue').length).toEqual(2);
  });

  it('the correct number of available options are 2 as one is selected already', () => {
    const defaultProps = {
      ...props,
      value: { label: 'Three', value: '3' },
    };
    const Select = mount(<ReactMultiSelect {...defaultProps} />);
    Select.find('DropdownIndicator').simulate('mouseDown', { button: 0 });
    expect(Select.find('Option').length).toEqual(2);
  });

  it('if minItems > 0, isRequired should be true', () => {
    const defaultProps = {
      ...props,
      minItems: 1,
    };
    const Select = mount(<ReactMultiSelect {...defaultProps} />);
    expect(Select.find('.form-element').hasClass('form-element--required')).toEqual(true);
  });
});
