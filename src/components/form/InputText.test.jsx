import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputText from './InputText';

const id = 'InputText';
const labelText = 'My Text Input';
const inputType = 'text';

describe('InputText', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const InputTextComponent = renderer.create(<InputText {...props} />).toJSON();
    expect(InputTextComponent).toMatchSnapshot();
  });

  it('has the correct type', () => {
    const props = {
      id,
      labelText,
    };
    const InputTextComponent = mount(<InputText {...props} />);
    expect(
      InputTextComponent.find('InputField')
        .find('input')
        .getDOMNode()
        .getAttribute('type'),
    ).toEqual(inputType);
  });
});
