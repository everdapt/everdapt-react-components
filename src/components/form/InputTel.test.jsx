import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputTel from './InputTel';

const id = 'InputTel';
const labelText = 'My Tel Input';
const inputType = 'tel';
const pattern = '[0-9]{10}';

describe('InputTel', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const InputTelComponent = renderer.create(<InputTel {...props} />).toJSON();
    expect(InputTelComponent).toMatchSnapshot();
  });

  it('has the correct type', () => {
    const props = {
      id,
      labelText,
    };
    const InputTelComponent = mount(<InputTel {...props} />);
    expect(
      InputTelComponent.find('InputField')
        .find('input')
        .getDOMNode()
        .getAttribute('type'),
    ).toEqual(inputType);
  });

  it('has the correct pattern', () => {
    const props = {
      id,
      labelText,
    };
    const InputTelComponent = mount(<InputTel {...props} />);
    expect(
      InputTelComponent.find('InputField')
        .find('input')
        .getDOMNode()
        .getAttribute('pattern'),
    ).toEqual(pattern);
  });
});
