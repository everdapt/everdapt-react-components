import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputSearch from './InputSearch';

const id = 'InputSearch';
const labelText = 'My Search Input';
const inputType = 'text';

describe('InputSearch', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const InputSearchComponent = renderer.create(<InputSearch {...props} />).toJSON();
    expect(InputSearchComponent).toMatchSnapshot();
  });

  it('has the correct type', () => {
    const props = {
      id,
      labelText,
    };
    const InputSearchComponent = mount(<InputSearch {...props} />);
    expect(
      InputSearchComponent.find('InputField')
        .find('input')
        .getDOMNode()
        .getAttribute('type'),
    ).toEqual(inputType);
  });
});
