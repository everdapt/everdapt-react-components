import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import ReactSelect from './ReactSelect';

const name = 'React Select';
const labelText = 'My Select';

const props = {
  name,
  value: { label: 'One', value: '1' },
  options: [{ label: 'One', value: '1' }, { label: 'Two', value: '2' }, { label: 'Three', value: '3' }],
  labelText,
  onChange: () => {},
};

describe('ReactSelect', () => {
  it('renders correctly', () => {
    const defaultProps = {
      ...props,
      onChange: null,
    };
    const Select = renderer.create(<ReactSelect {...defaultProps} />).toJSON();
    expect(Select).toMatchSnapshot();
  });

  it('the correct option is selected by default', () => {
    const Select = mount(<ReactSelect {...props} />);
    expect(Select.find('SingleValue>div').text()).toEqual('One');
  });

  it('the correct number of options is 3', () => {
    const Select = mount(<ReactSelect {...props} />);
    Select.find('DropdownIndicator').simulate('mouseDown', { button: 0 });
    expect(Select.find('Option').length).toEqual(3);
  });
});
