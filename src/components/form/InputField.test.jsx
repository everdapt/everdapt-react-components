import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputField from './InputField';

const id = 'InputComponent';
const fieldType = 'text';
const placeholder = 'My PlaceHolder';

describe('InputField', () => {
  it('renders correctly', () => {
    const props = {
      id,
      type: fieldType,
    };
    const InputComponent = renderer.create(<InputField {...props} />).toJSON();
    expect(InputComponent).toMatchSnapshot();
  });

  it('should have the appropriate props', () => {
    const props = {
      id,
      type: fieldType,
    };
    const InputComponent = mount(<InputField {...props} />);
    expect(InputComponent.props().type).toEqual(fieldType);
    expect(InputComponent.props().id).toEqual(id);
    expect(InputComponent.props().hasGlobalError).toEqual(false);
    expect(InputComponent.props().errorText).toBeNull();
    expect(InputComponent.props().labelText).toBeNull();
    expect(InputComponent.props().hintText).toBeNull();
    expect(InputComponent.props().onChange).toBeNull();
    expect(InputComponent.props().placeholder).toBeNull();
    expect(InputComponent.props().isRequired).toEqual(false);
  });

  it('should have placeholder attribute if provided', () => {
    const props = {
      id,
      type: fieldType,
      placeholder,
    };
    const placeholderVal = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('placeholder');
    expect(placeholderVal).toEqual(placeholder);
  });

  it('should not have placeholder attribute if none provided', () => {
    const props = {
      id,
      type: fieldType,
    };
    const placeholderVal = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('placeholder');
    expect(placeholderVal).toBeNull();
  });

  it('should be required if isRequired = true', () => {
    const props = {
      id,
      type: fieldType,
      isRequired: true,
    };
    const isRequired = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('required');
    expect(isRequired).toBeDefined();
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      id,
      type: fieldType,
    };
    const isRequired = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('required');
    expect(isRequired).toBeNull();
  });

  it('should not be required if isRequired = false', () => {
    const props = {
      id,
      type: fieldType,
      isRequired: false,
    };
    const isRequired = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('required');
    expect(isRequired).toBeDefined();
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      type: fieldType,
      isDisabled: true,
    };
    const isDisabled = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeDefined();
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      id,
      type: fieldType,
    };
    const isDisabled = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeNull();
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      type: fieldType,
      isDisabled: false,
    };
    const isDisabled = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeDefined();
  });

  it('should have error class if hasGlobalError = true', () => {
    const props = {
      id,
      type: fieldType,
      hasGlobalError: true,
    };
    const InputComponent = mount(<InputField {...props} />).find('.form-element');
    expect(InputComponent.hasClass('form-element--error')).toEqual(true);
  });

  it('should have error class if errorText provided', () => {
    const errorText = 'My Error';
    const props = {
      id,
      type: fieldType,
      errorText,
    };
    const InputComponent = mount(<InputField {...props} />).find('.form-element');
    expect(InputComponent.hasClass('form-element--error')).toEqual(true);
  });

  it('should have error message if errorText provided', () => {
    const errorText = 'My Error';
    const props = {
      id,
      type: fieldType,
      errorText,
    };
    const InputComponentError = mount(<InputField {...props} />)
      .find('.form-element')
      .find('.form-element-error-msg');
    expect(InputComponentError).toHaveLength(1);
    expect(InputComponentError.text()).toEqual(errorText);
  });

  it('should have label if labelText provided', () => {
    const labelText = 'My Component Label';
    const props = {
      id,
      type: fieldType,
      labelText,
    };
    const InputComponentLabel = mount(<InputField {...props} />)
      .find('.form-element')
      .find('label');
    expect(InputComponentLabel).toHaveLength(1);
    expect(InputComponentLabel.text()).toEqual(labelText);
  });

  it('should not have label if no labelText provided', () => {
    const props = {
      id,
      type: fieldType,
    };
    const InputComponentLabel = mount(<InputField {...props} />)
      .find('.form-element')
      .find('label');
    expect(InputComponentLabel).toHaveLength(0);
  });

  it('should have hint if hintText provided', () => {
    const hintText = 'My Component Hint';
    const props = {
      id,
      type: fieldType,
      hintText,
    };
    const InputComponentHint = mount(<InputField {...props} />)
      .find('.form-element')
      .find('.form-element-hint');
    expect(InputComponentHint).toHaveLength(1);
    expect(InputComponentHint.text()).toEqual(hintText);
  });

  it('should not have hint if no hintText provided', () => {
    const props = {
      id,
      type: fieldType,
    };
    const InputComponentHint = mount(<InputField {...props} />)
      .find('.form-element')
      .find('.form-element-hint');
    expect(InputComponentHint).toHaveLength(0);
  });

  it('should have aria attribute if hintText provided', () => {
    const hintText = 'My Component Hint';
    const props = {
      id,
      type: fieldType,
      hintText,
    };
    const AriaAttr = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('aria-describedby');
    expect(AriaAttr).toBeDefined();
    expect(AriaAttr).toEqual(`${id}-hint`);
  });

  it('should not have aria attribute if no hintText provided', () => {
    const props = {
      id,
      type: fieldType,
    };
    const AriaAttr = mount(<InputField {...props} />)
      .find('input')
      .getDOMNode()
      .getAttribute('aria-describedby');
    expect(AriaAttr).toBeNull();
  });

  it('should not have pattern attribute if no pattern provided', () => {
    const props = {
      id,
      type: fieldType,
    };
    const InputFieldComponent = mount(<InputField {...props} />);
    expect(
      InputFieldComponent.find('input')
        .getDOMNode()
        .getAttribute('pattern'),
    ).toBeNull();
  });

  it('should have pattern attribute if pattern provided', () => {
    const pattern = '[0-9]';
    const props = {
      id,
      type: fieldType,
      pattern,
    };
    const InputFieldComponent = mount(<InputField {...props} />);
    expect(
      InputFieldComponent.find('input')
        .getDOMNode()
        .getAttribute('pattern'),
    ).toEqual(pattern);
  });

  // TODO: test onChange
});
