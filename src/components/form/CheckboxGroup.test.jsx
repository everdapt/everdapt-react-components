import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import ReactTestUtils from 'react-dom/test-utils';
import CheckboxGroup from './CheckboxGroup';

const id = 'CheckboxGroup';
const labelText = 'How do you want to check for permissions?';
const hintText = 'You can check any of the permission checks below to check for permissions';
const onChange = () => {};
const list = [
  {
    label: 'By checking for permissions',
    value: 'By checking for permissions',
    hintText: 'Check this box to signify you want to check this box',
  },
  {
    label: 'By permitting the checks',
    value: 'By permitting the checks',
  },
  {
    label: 'By permitting the checks for permissions',
    value: 'By permitting the checks for permissions',
  },
];

const values = ['By permitting the checks'];

describe('CheckboxGroup', () => {
  it('renders correctly', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
    };
    const CheckboxGroupComponent = renderer.create(<CheckboxGroup {...props} />).toJSON();
    expect(CheckboxGroupComponent).toMatchSnapshot();
  });

  it('should be required if isRequired = true', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
      isRequired: true,
    };
    const isRequired = mount(<CheckboxGroup {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(true);
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
    };
    const isRequired = mount(<CheckboxGroup {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should not be required if isRequired = false', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
    };
    const isRequired = mount(<CheckboxGroup {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
      isDisabled: true,
    };
    const isDisabled = mount(<CheckboxGroup {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(true);
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
    };
    const isDisabled = mount(<CheckboxGroup {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(false);
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
      isDisabled: false,
    };
    const isDisabled = mount(<CheckboxGroup {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(false);
  });

  it('should shows none of above is checked when values is empty', () => {
    const noneOfAboveText = 'None of the above';
    const props = {
      id,
      list,
      values: [noneOfAboveText],
      labelText,
      hintText,
      onChange,
      noneOfAbove: true,
      noneOfAboveText,
    };
    const CheckboxGroupComponent = mount(<CheckboxGroup {...props} />);
    expect(
      CheckboxGroupComponent.find(`#${id}-${list.length + 1}`)
        .getDOMNode()
        .getAttribute('checked'),
    ).toBeDefined();
  });

  it('should shows error when is required and values is empty', () => {
    const props = {
      id,
      list,
      labelText,
      hintText,
      onChange,
      isRequired: true,
    };
    const CheckboxGroupComponent = mount(<CheckboxGroup {...props} />);
    expect(CheckboxGroupComponent.find('fieldset').hasClass('form-group--error')).toEqual(true);
  });

  it('should empty values if none of above is checked', () => {
    const noneOfAboveText = 'None of the above';
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange,
      noneOfAbove: true,
      noneOfAboveText,
    };
    const CheckboxGroupComponent = mount(<CheckboxGroup {...props} />);
    const node = CheckboxGroupComponent.find('input')
      .last()
      .getDOMNode();
    ReactTestUtils.Simulate.change(node, {
      target: {
        checked: true,
        value: noneOfAboveText,
      },
    });
    expect(CheckboxGroupComponent.state().values).toEqual([noneOfAboveText]);
  });

  it('should uncheck none of above if some checkbox would be checked', () => {
    const noneOfAboveText = 'None of the above';
    const props = {
      id,
      list,
      values: [noneOfAboveText],
      labelText,
      hintText,
      onChange,
      noneOfAbove: true,
      noneOfAboveText,
    };
    const CheckboxGroupComponent = mount(<CheckboxGroup {...props} />);
    const firstCheckbox = CheckboxGroupComponent.find('input')
      .first()
      .getDOMNode();
    ReactTestUtils.Simulate.change(firstCheckbox, {
      target: {
        checked: true,
      },
    });
    expect(CheckboxGroupComponent.find(`#${id}-${list.length + 1}[checked="checked"]`)).toHaveLength(0);
  });

  it('should uncheck none of above if some checkbox would be checked', () => {
    const noneOfAboveText = 'None of the above';
    const props = {
      id,
      list,
      values: [noneOfAboveText],
      labelText,
      hintText,
      onChange,
      noneOfAbove: true,
      noneOfAboveText,
    };
    const CheckboxGroupComponent = mount(<CheckboxGroup {...props} />);
    const firstCheckbox = CheckboxGroupComponent.find('input')
      .first()
      .getDOMNode();
    ReactTestUtils.Simulate.change(firstCheckbox, {
      target: {
        checked: true,
      },
    });
    expect(CheckboxGroupComponent.find(`#${id}-${list.length + 1}[checked="checked"]`)).toHaveLength(0);
  });

  it('should pass checked values to onChange prop function', () => {
    let expectedValues = [];
    const onChangeFunc = vals => {
      expectedValues = vals;
    };
    const props = {
      id,
      list,
      values,
      labelText,
      hintText,
      onChange: onChangeFunc,
      noneOfAbove: true,
    };
    const CheckboxGroupComponent = mount(<CheckboxGroup {...props} />);
    const firstCheckbox = CheckboxGroupComponent.find('input')
      .first()
      .getDOMNode();
    ReactTestUtils.Simulate.change(firstCheckbox, {
      target: {
        checked: true,
        value: 'By checking for permissions',
      },
    });
    expect(expectedValues).toEqual(['By permitting the checks', 'By checking for permissions']);
  });
});
