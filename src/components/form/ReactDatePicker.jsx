import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-date-picker/dist/entry.nostyle';
import { DateTime } from 'luxon';

class ReactDatePicker extends PureComponent {
  /* eslint-disable react/forbid-prop-types */
  static propTypes = {
    id: PropTypes.string.isRequired,
    labelText: PropTypes.string,
    hintText: PropTypes.string,
    errorText: PropTypes.string,
    value: PropTypes.object,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    minDate: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
    locale: PropTypes.string,
    hasGlobalError: PropTypes.bool,
    onChange: PropTypes.func,
  };
  /* eslint-enable */

  static defaultProps = {
    labelText: null,
    hintText: null,
    errorText: null,
    value: null,
    isRequired: false,
    isDisabled: false,
    minDate: DateTime.fromJSDate(new Date())
      .minus({ years: 100 })
      .toJSDate(),
    maxDate: DateTime.fromJSDate(new Date())
      .minus({ days: 1 })
      .toJSDate(),
    locale: 'en-US',
    hasGlobalError: false,
    onChange: null,
  };

  render() {
    const {
      id,
      labelText,
      hintText,
      errorText,
      value,
      isRequired,
      isDisabled,
      minDate,
      maxDate,
      locale,
      hasGlobalError,
      onChange,
    } = this.props;
    /* eslint-disable jsx-a11y/label-has-for */
    const classNames = ['form-element', 'form-element--datepicker'];
    if (hasGlobalError || errorText) classNames.push('form-element--error');
    if (isRequired) classNames.push('form-element--required');
    const dateProps = {
      id,
      name: id,
      required: isRequired,
      disabled: isDisabled,
      minDate,
      maxDate,
      locale,
      onChange,
      value,
    };

    return (
      <div className={`${classNames.join(' ')}`}>
        {labelText && (
          <label className="label" htmlFor={id} id={`${id}-label`}>
            {labelText}
            {isRequired && <span className="required">*</span>}
          </label>
        )}
        {hintText && (
          <span id={`${id}-hint`} className="form-element-hint">
            {hintText}
          </span>
        )}
        {errorText && <span className="form-element-error-msg">{errorText}</span>}
        <DatePicker {...dateProps} />
      </div>
    );
    /* eslint-enable */
  }
}

export default ReactDatePicker;
