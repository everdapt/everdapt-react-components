import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InputField from './InputField';

class InputTel extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    labelText: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    pattern: PropTypes.string,
    hintText: PropTypes.string,
    errorText: PropTypes.string,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    hintText: null,
    placeholder: null,
    value: undefined,
    pattern: '[0-9]{10}',
    errorText: null,
    isRequired: false,
    isDisabled: false,
    onChange: null,
  };

  render() {
    return <InputField {...this.props} type="tel" />;
  }
}

export default InputTel;
