import React, { Component } from 'react';
import PropTypes from 'prop-types';
import zxcvbnAsync from 'zxcvbn-async';
import InputPassword from './InputPassword';

const zxcvbnPromose = zxcvbnAsync.load({ sync: false });
const isTooShort = (password, minLength) => password.length < minLength;
const scoreWords = ['Weak', 'Okay', 'Good', 'Strong', 'Great'];
const tooShortWord = 'Too short';
const userInputs = [];
const hint = 'Suggest at least 1 or 2 uppercase letters and/or numbers.';

class InputPasswordStrength extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    hintText: PropTypes.string,
    labelText: PropTypes.string,
    placeholder: PropTypes.string,
    errorText: PropTypes.string,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    showByDefault: PropTypes.bool,
    hasGlobalError: PropTypes.bool,
    changeCallback: PropTypes.func,
    minLength: PropTypes.number,
    minScore: PropTypes.number,
  };

  static defaultProps = {
    placeholder: null,
    hasGlobalError: false,
    changeCallback: null,
    minLength: 8,
    minScore: 2,
    hintText: hint,
    labelText: null,
    errorText: null,
    isRequired: false,
    isDisabled: false,
    showByDefault: false,
  };

  state = {
    score: 0,
    isValid: false,
    password: '',
  };

  handleChange = e => {
    const password = e.target.value;
    const { changeCallback, minScore, minLength } = this.props;

    let score = 0;

    if (!isTooShort(password, minLength)) {
      zxcvbnPromose.then(zxcvbn => {
        const result = zxcvbn(password, userInputs);
        score = result.score === -1 ? 0 : result.score;
      });
    }

    this.setState(
      {
        isValid: score >= minScore,
        password,
        score,
      },
      () => {
        if (changeCallback) {
          changeCallback(this.state);
        }
      },
    );
  };

  render() {
    const { score, password, isValid } = this.state;
    const { minScore, minLength, changeCallback, hintText, ...rest } = this.props;
    const strengthDesc = isTooShort(password, minLength) ? tooShortWord : scoreWords[score];
    /* eslint-disable no-nested-ternary */
    const strengthClass = isValid ? 'is-password-valid' : password.length ? 'is-password-invalid' : null;
    /* eslint-enable */
    return (
      <InputPassword
        {...rest}
        hintText={`Must be ${minLength} characters minimum. ${hintText}`}
        shouldCheckStrength
        strengthState={strengthDesc}
        strengthClass={strengthClass}
        onChange={this.handleChange}
      />
    );
  }
}

export default InputPasswordStrength;
