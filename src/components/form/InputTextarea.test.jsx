import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputTextarea from './InputTextarea';

const id = 'InputTextareaComponent';

describe('InputTextarea', () => {
  it('renders correctly', () => {
    const props = {
      id,
    };
    const InputTextareaComp = renderer.create(<InputTextarea {...props} />).toJSON();
    expect(InputTextareaComp).toMatchSnapshot();
  });

  it('should have the appropriate props', () => {
    const props = {
      id,
    };
    const InputTextareaComp = mount(<InputTextarea {...props} />);
    expect(InputTextareaComp.props().id).toEqual(id);
    expect(InputTextareaComp.props().labelText).toBeNull();
    expect(InputTextareaComp.props().hintText).toBeNull();
    expect(InputTextareaComp.props().onChange).toBeNull();
    expect(InputTextareaComp.props().isDisabled).toEqual(false);
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      isDisabled: true,
    };
    const isDisabled = mount(<InputTextarea {...props} />)
      .find('textarea')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeDefined();
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      id,
    };
    const isDisabled = mount(<InputTextarea {...props} />)
      .find('textarea')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeNull();
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      isDisabled: false,
    };
    const isDisabled = mount(<InputTextarea {...props} />)
      .find('textarea')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeDefined();
  });

  it('should have label if labelText provided', () => {
    const labelText = 'My Component Label';
    const props = {
      id,
      labelText,
    };
    const InputTextareaLabel = mount(<InputTextarea {...props} />)
      .find('.form-element')
      .find('label');
    expect(InputTextareaLabel).toHaveLength(1);
    expect(InputTextareaLabel.text()).toEqual(labelText);
  });

  it('should not have label if no labelText provided', () => {
    const props = {
      id,
    };
    const InputTextareaLabel = mount(<InputTextarea {...props} />)
      .find('.form-element')
      .find('label');
    expect(InputTextareaLabel).toHaveLength(0);
  });

  it('should have hint if hintText provided', () => {
    const hintText = 'My Component Hint';
    const props = {
      id,
      hintText,
    };
    const InputTextareaHint = mount(<InputTextarea {...props} />)
      .find('.form-element')
      .find('.form-element-hint');
    expect(InputTextareaHint).toHaveLength(1);
    expect(InputTextareaHint.text()).toEqual(hintText);
  });

  it('should not have hint if no hintText provided', () => {
    const props = {
      id,
    };
    const InputTextareaHint = mount(<InputTextarea {...props} />)
      .find('.form-element')
      .find('.form-element-hint');
    expect(InputTextareaHint).toHaveLength(0);
  });

  it('should have aria attribute if hintText provided', () => {
    const hintText = 'My Component Hint';
    const props = {
      id,
      hintText,
    };
    const AriaAttr = mount(<InputTextarea {...props} />)
      .find('textarea')
      .getDOMNode()
      .getAttribute('aria-describedby');
    expect(AriaAttr).toBeDefined();
    expect(AriaAttr).toEqual(`${id}-hint`);
  });

  it('should not have aria attribute if no hintText provided', () => {
    const props = {
      id,
    };
    const AriaAttr = mount(<InputTextarea {...props} />)
      .find('textarea')
      .getDOMNode()
      .getAttribute('aria-describedby');
    expect(AriaAttr).toBeNull();
  });

  // TODO: test onChange
});
