import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import Toggle from './Toggle';

const id = 'Toggle';
const labelText = 'My Toggle';
const onChange = () => {};

describe('Toggle', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
      isChecked: false,
      onChange,
    };
    const ToggleComponent = renderer.create(<Toggle {...props} />).toJSON();
    expect(ToggleComponent).toMatchSnapshot();
  });

  it('should have the appropriate props', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      onChange,
    };
    const ToggleComponent = mount(<Toggle {...props} />);
    expect(ToggleComponent.props().id).toEqual(id);
    expect(ToggleComponent.props().labelText).toEqual(labelText);
    expect(ToggleComponent.props().isChecked).toEqual(true);
  });

  it('should be checked if isChecked = true', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      onChange,
    };
    const ToggleComponent = mount(<Toggle {...props} />);
    expect(
      ToggleComponent.find('input')
        .getDOMNode()
        .getAttribute('checked'),
    ).toBeDefined();
  });

  it('should not be checked if isChecked = false', () => {
    const props = {
      id,
      labelText,
      isChecked: false,
      onChange,
    };
    const ToggleComponent = mount(<Toggle {...props} />);
    expect(
      ToggleComponent.find('input')
        .getDOMNode()
        .getAttribute('checked'),
    ).toBeNull();
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      isDisabled: true,
      onChange,
    };
    const ToggleComponent = mount(<Toggle {...props} />);
    expect(
      ToggleComponent.find('input')
        .getDOMNode()
        .getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      isDisabled: false,
      onChange,
    };
    const ToggleComponent = mount(<Toggle {...props} />);
    expect(
      ToggleComponent.find('input')
        .getDOMNode()
        .getAttribute('disabled'),
    ).toBeNull();
  });

  // TODO: test onChange, test isInline
});
