import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import InputCheckbox from './InputCheckbox';

const id = 'InputCheckbox';
const labelText = 'My Input Checkbox';
const onChange = () => {};

describe('InputCheckbox', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
      isChecked: false,
      onChange,
    };
    const InputCheckboxComponent = renderer.create(<InputCheckbox {...props} />).toJSON();
    expect(InputCheckboxComponent).toMatchSnapshot();
  });

  it('should have the appropriate props', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      onChange,
    };
    const InputCheckboxComponent = mount(<InputCheckbox {...props} />);
    expect(InputCheckboxComponent.props().id).toEqual(id);
    expect(InputCheckboxComponent.props().labelText).toEqual(labelText);
    expect(InputCheckboxComponent.props().isChecked).toEqual(true);
  });

  it('should be checked if isChecked = true', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      onChange,
    };
    const InputCheckboxComponent = mount(<InputCheckbox {...props} />);
    expect(
      InputCheckboxComponent.find('input')
        .getDOMNode()
        .getAttribute('checked'),
    ).toBeDefined();
  });

  it('should not be checked if isChecked = false', () => {
    const props = {
      id,
      labelText,
      isChecked: false,
      onChange,
    };
    const InputCheckboxComponent = mount(<InputCheckbox {...props} />);
    expect(
      InputCheckboxComponent.find('input')
        .getDOMNode()
        .getAttribute('checked'),
    ).toBeNull();
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      isDisabled: true,
      onChange,
    };
    const InputCheckboxComponent = mount(<InputCheckbox {...props} />);
    expect(
      InputCheckboxComponent.find('input')
        .getDOMNode()
        .getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      labelText,
      isChecked: true,
      isDisabled: false,
      onChange,
    };
    const InputCheckboxComponent = mount(<InputCheckbox {...props} />);
    expect(
      InputCheckboxComponent.find('input')
        .getDOMNode()
        .getAttribute('disabled'),
    ).toBeNull();
  });
});
