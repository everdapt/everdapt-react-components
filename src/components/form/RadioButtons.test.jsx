import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import ReactTestUtils from 'react-dom/test-utils';
import RadioButtons from './RadioButtons';

const name = 'CheckboxGroup';
const labelText = 'How do you want to check for permissions?';
const hintText = 'You can check any of the permission checks below to check for permissions';
const onChange = () => {};
const list = [
  {
    label: 'Share all medical information (imaging reports) and billing information (payment details)',
    value: 'hipaaAllInfo',
    hintText: 'Check this box to signify you want to check this box',
  },
  {
    label: 'Share all medical information but do not share billing information',
    value: 'hipaaOnlyMedical',
  },
  {
    label: 'Do not share anything',
    value: 'hipaaNoInfo',
  },
];

const value = 'hipaaOnlyMedical';

describe('RadioButtons', () => {
  it('renders correctly', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
    };
    const RadioButtonsComponent = renderer.create(<RadioButtons {...props} />).toJSON();
    expect(RadioButtonsComponent).toMatchSnapshot();
  });

  it('should be required if isRequired = true', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
      isRequired: true,
    };
    const isRequired = mount(<RadioButtons {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(true);
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
    };
    const isRequired = mount(<RadioButtons {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should not be required if isRequired = false', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
      isRequired: false,
    };
    const isRequired = mount(<RadioButtons {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
      isDisabled: true,
    };
    const isDisabled = mount(<RadioButtons {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(true);
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
    };
    const isDisabled = mount(<RadioButtons {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(false);
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
      isDisabled: false,
    };
    const isDisabled = mount(<RadioButtons {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(false);
  });

  it('should be checked radio button with id = CheckboxGroup-2', () => {
    const props = {
      name,
      list,
      value,
      labelText,
      hintText,
      onChange,
    };
    const RadioButtonsComponent = mount(<RadioButtons {...props} />);
    expect(
      RadioButtonsComponent.find('#CheckboxGroup-2')
        .getDOMNode()
        .getAttribute('checked'),
    ).toBeDefined();
  });

  it('should shows error when is required and value is empty', () => {
    const props = {
      name,
      list,
      labelText,
      hintText,
      onChange,
      isRequired: true,
    };
    const RadioButtonsComponent = mount(<RadioButtons {...props} />);
    expect(RadioButtonsComponent.find('fieldset').hasClass('form-group--error')).toEqual(true);
  });

  it('should pass checked value to onChange prop function', () => {
    let expectedValues = '';
    const selectedVal = 'hipaaAllInfo';
    const onChangeFunc = val => {
      expectedValues = val;
    };
    const props = {
      name,
      list,
      labelText,
      hintText,
      onChange: onChangeFunc,
    };
    const CheckboxGroupComponent = mount(<RadioButtons {...props} />);
    const firstCheckbox = CheckboxGroupComponent.find('input')
      .first()
      .getDOMNode();
    ReactTestUtils.Simulate.change(firstCheckbox, {
      target: {
        checked: true,
        value: selectedVal,
      },
    });
    expect(expectedValues).toEqual(selectedVal);
  });
});
