import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const inputTypeMapping = {
  text: 'text',
  email: 'email',
  search: 'text',
  tel: 'tel',
};

class InputField extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    labelText: PropTypes.string,
    hintText: PropTypes.string,
    errorText: PropTypes.string,
    value: PropTypes.string,
    pattern: PropTypes.string,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    hasGlobalError: PropTypes.bool,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    placeholder: null,
    labelText: null,
    hintText: null,
    errorText: null,
    value: undefined,
    pattern: null,
    isRequired: false,
    isDisabled: false,
    hasGlobalError: false,
    onChange: null,
  };

  render() {
    const {
      id,
      type,
      placeholder,
      labelText,
      hintText,
      errorText,
      value,
      isRequired,
      isDisabled,
      hasGlobalError,
      onChange,
      pattern,
    } = this.props;
    /* eslint-disable jsx-a11y/label-has-for */
    const classNames = ['form-element', `form-element--${type}`];
    if (hasGlobalError || errorText) classNames.push('form-element--error');
    if (isRequired) classNames.push('form-element--required');
    const inputProps = {
      id,
      name: id,
      type: inputTypeMapping[type] || 'text',
      required: isRequired,
      disabled: isDisabled,
      onChange,
      placeholder: placeholder || undefined,
      value,
      pattern: pattern || undefined,
      ...(hintText && { 'aria-describedby': `${id}-hint` }),
    };
    return (
      <div className={`${classNames.join(' ')}`}>
        {labelText && (
          <label className="label" htmlFor={id} id={`${id}-label`}>
            {labelText}
            {isRequired && <span className="required">*</span>}
          </label>
        )}
        {hintText && (
          <span id={`${id}-hint`} className="form-element-hint">
            {hintText}
          </span>
        )}
        {errorText && <span className="form-element-error-msg">{errorText}</span>}
        <input {...inputProps} />
      </div>
    );
    /* eslint-enable */
  }
}

export default InputField;
