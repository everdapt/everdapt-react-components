import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import ReactTestUtils from 'react-dom/test-utils';
import InputPin from './InputPin';

const id = 'InputPin';
const labelText = 'My Input Pin';
const hintText = 'My hint text';
const invalidNumber = 'Invalid number.';

describe('InputPin', () => {
  it('renders correctly', () => {
    const props = {
      id: 'pin',
      labelText: 'Enter PIN',
      isRequired: true,
    };
    const InputComponent = renderer.create(<InputPin {...props} />).toJSON();
    expect(InputComponent).toMatchSnapshot();
  });

  it('should have the appropriate props', () => {
    const props = {
      id,
      labelText,
      hintText,
    };
    const InputPinComponent = mount(<InputPin {...props} />);
    expect(InputPinComponent.props().id).toEqual(id);
    expect(InputPinComponent.props().labelText).toEqual(labelText);
    expect(InputPinComponent.props().hintText).toEqual(hintText);
  });

  it('should renders inputs count equal to length prop', () => {
    const length = 7;
    const props = {
      id,
      labelText,
      length,
    };
    const InputPinComponent = mount(<InputPin {...props} />);
    expect(InputPinComponent.find('input').length).toEqual(length);
  });

  it('should be required if isRequired = true', () => {
    const props = {
      id,
      labelText,
      isRequired: true,
    };
    const isRequired = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(true);
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      id,
      labelText,
    };
    const isRequired = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should be required if isRequired = true', () => {
    const props = {
      id,
      labelText,
      isRequired: true,
    };
    const isRequired = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(true);
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      id,
      labelText,
    };
    const isRequired = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should not be required if isRequired = false', () => {
    const props = {
      id,
      labelText,
      isRequired: false,
    };
    const isRequired = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--required');
    expect(isRequired).toEqual(false);
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      labelText,
      isDisabled: true,
    };
    const isDisabled = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(true);
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      id,
      labelText,
    };
    const isDisabled = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(false);
  });

  it('should not be disabled if isDisabled = false', () => {
    const props = {
      id,
      labelText,
      isDisabled: false,
    };
    const isDisabled = mount(<InputPin {...props} />)
      .find('fieldset')
      .hasClass('form-group--disabled');
    expect(isDisabled).toEqual(false);
  });

  it('should be first input focused if focusOnLoad = true', () => {
    const props = {
      id,
      labelText,
      focusOnLoad: true,
    };
    document.body.innerHTML = '<div></div>';
    const InputPinComponent = mount(<InputPin {...props} />, {
      attachTo: document.getElementsByName('div')[0],
    });
    expect(
      InputPinComponent.find('input')
        .first()
        .props().id,
    ).toEqual(document.activeElement.id);
  });

  it('should be first input focused if focusOnLoad is omitted', () => {
    const props = {
      id,
      labelText,
    };
    document.body.innerHTML = '<div></div>';
    const InputPinComponent = mount(<InputPin {...props} />, {
      attachTo: document.getElementsByName('div')[0],
    });
    expect(
      InputPinComponent.find('input')
        .first()
        .props().id,
    ).toEqual(document.activeElement.id);
  });

  it('should shows error when entered non digital value', () => {
    const props = {
      id,
      labelText,
    };
    const InputComponent = mount(<InputPin {...props} />);
    const node = InputComponent.find('input')
      .first()
      .getDOMNode();
    node.value = 'a';
    ReactTestUtils.Simulate.keyDown(node);
    ReactTestUtils.Simulate.blur(node);
    expect(InputComponent.state().error).toEqual(invalidNumber);
  });

  it('should not shows error when entered digital value', () => {
    const props = {
      id,
      labelText,
    };
    const InputComponent = mount(<InputPin {...props} />);
    const node = InputComponent.find('input')
      .first()
      .getDOMNode();
    node.value = '1';
    ReactTestUtils.Simulate.keyDown(node);
    ReactTestUtils.Simulate.blur(node);
    expect(InputComponent.state().error).toEqual('');
  });

  it('should pass validated value to getValue prop function', () => {
    const enteredValue = '1';
    let expectedValue = '';
    const getValue = value => {
      expectedValue = value;
    };
    const props = {
      id,
      labelText,
      getValue,
    };

    const InputComponent = mount(<InputPin {...props} />);
    const node = InputComponent.find('input')
      .first()
      .getDOMNode();
    node.value = enteredValue;
    ReactTestUtils.Simulate.keyDown(node);
    ReactTestUtils.Simulate.blur(node);
    expect(expectedValue).toEqual(enteredValue);
  });

  it('should not pass non validated value to getValue prop function', () => {
    const enteredValue = 'a';
    let expectedValue = '';
    const getValue = value => {
      expectedValue = value;
    };
    const props = {
      id,
      labelText,
      getValue,
    };

    const InputComponent = mount(<InputPin {...props} />);
    const node = InputComponent.find('input')
      .first()
      .getDOMNode();
    node.value = enteredValue;
    ReactTestUtils.Simulate.keyDown(node);
    ReactTestUtils.Simulate.blur(node);
    expect(expectedValue).toEqual('');
  });
});
