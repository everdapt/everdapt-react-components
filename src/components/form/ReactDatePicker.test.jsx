import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { DateTime } from 'luxon';
import ReactDatePicker from './ReactDatePicker';

const id = 'DatePicker';
const labelText = 'My Date Picker';

describe('ReactDatePicker', () => {
  it('renders correctly', () => {
    const props = {
      id,
      labelText,
    };
    const DatePickerComponent = renderer.create(<ReactDatePicker {...props} />).toJSON();
    expect(DatePickerComponent).toMatchSnapshot();
  });

  it('should be required if isRequired = true', () => {
    const props = {
      id,
      isRequired: true,
    };
    const isRequired = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('required');
    expect(isRequired).toBeDefined();
  });

  it('should not be required if isRequired is omitted', () => {
    const props = {
      id,
    };
    const isRequired = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('required');
    expect(isRequired).toBeNull();
  });

  it('should not be required if isRequired = false', () => {
    const props = {
      id,
      isRequired: false,
    };
    const isRequired = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('required');
    expect(isRequired).toBeDefined();
  });

  it('should be disabled if isDisabled = true', () => {
    const props = {
      id,
      isDisabled: true,
    };
    const isDisabled = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeDefined();
  });

  it('should not be disabled if isDisabled is omitted', () => {
    const props = {
      id,
    };
    const isDisabled = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('disabled');
    expect(isDisabled).toBeNull();
  });

  it('should have error class if hasGlobalError = true', () => {
    const props = {
      id,
      hasGlobalError: true,
    };
    const DateComponent = mount(<ReactDatePicker {...props} />).find('.form-element');
    expect(DateComponent.hasClass('form-element--error')).toEqual(true);
  });

  it('should have error class if errorText provided', () => {
    const errorText = 'My Error';
    const props = {
      id,
      errorText,
    };
    const DateComponent = mount(<ReactDatePicker {...props} />).find('.form-element');
    expect(DateComponent.hasClass('form-element--error')).toEqual(true);
  });

  it('should have error message if errorText provided', () => {
    const errorText = 'My Error';
    const props = {
      id,
      errorText,
    };
    const DateComponentError = mount(<ReactDatePicker {...props} />)
      .find('.form-element')
      .find('.form-element-error-msg');
    expect(DateComponentError).toHaveLength(1);
    expect(DateComponentError.text()).toEqual(errorText);
  });

  it('should have label if labelText provided', () => {
    const props = {
      id,
      labelText,
    };
    const DateComponentLabel = mount(<ReactDatePicker {...props} />)
      .find('.form-element')
      .find('label');
    expect(DateComponentLabel).toHaveLength(1);
    expect(DateComponentLabel.text()).toEqual(labelText);
  });

  it('should not have label if no labelText provided', () => {
    const props = {
      id,
    };
    const DateComponentLabel = mount(<ReactDatePicker {...props} />)
      .find('.form-element')
      .find('label');
    expect(DateComponentLabel).toHaveLength(0);
  });

  it('should be minDate today minus 100 years, and maxDate today minus a day', () => {
    const minDate = DateTime.fromJSDate(new Date())
      .minus({ years: 100 })
      .toFormat('yyyy-MM-dd');
    const maxDate = DateTime.fromJSDate(new Date())
      .minus({ days: 1 })
      .toFormat('yyyy-MM-dd');

    const props = {
      id,
    };
    const minDateProp = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('min');
    const maxDateProp = mount(<ReactDatePicker {...props} />)
      .find('input[type="date"]')
      .getDOMNode()
      .getAttribute('max');
    expect(minDateProp).toEqual(minDate);
    expect(maxDateProp).toEqual(maxDate);
  });
});
