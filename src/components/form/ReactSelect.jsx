import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

const styles = {
  clearIndicator: () => ({}),
  dropdownIndicator: () => ({}),
  container: () => ({}),
  control: () => ({}),
  group: () => ({}),
  groupHeading: () => ({}),
  indicatorsContainer: () => ({}),
  indicatorSeparator: () => ({}),
  input: () => ({}),
  loadingIndicator: () => ({}),
  loadingMessage: () => ({}),
  menu: () => ({}),
  menuList: () => ({}),
  menuPortal: () => ({}),
  multiValue: () => ({}),
  multiValueLabel: () => ({}),
  multiValueRemove: () => ({}),
  noOptionsMessage: () => ({}),
  option: () => ({}),
  placeholder: () => ({}),
  singleValue: () => ({}),
  valueContainer: () => ({}),
};

class ReactSelect extends PureComponent {
  /* eslint-disable react/forbid-prop-types */
  static propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array,
    value: PropTypes.object,
    labelText: PropTypes.string,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    hintText: PropTypes.string,
    errorText: PropTypes.string,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isSearchable: PropTypes.bool,
    isClearable: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onChange: PropTypes.func,
  };
  /* eslint-enable */

  static defaultProps = {
    options: null,
    value: null,
    labelText: null,
    className: null,
    placeholder: null,
    hintText: null,
    errorText: null,
    isRequired: false,
    isDisabled: false,
    isSearchable: true,
    isClearable: true,
    autoFocus: false,
    onChange: null,
  };

  render() {
    const {
      name,
      options,
      value,
      labelText,
      placeholder,
      hintText,
      errorText,
      isRequired,
      isDisabled,
      isSearchable,
      isClearable,
      autoFocus,
      onChange,
    } = this.props;

    const classNames = ['form-element', 'form-element--singleselect'];

    if (errorText) classNames.push('form-element--error');
    if (isRequired) classNames.push('form-element--required');

    const singleProps = {
      name,
      options,
      value,
      classNamePrefix: 'react-select',
      className: 'input--react-select',
      placeholder,
      isDisabled,
      isSearchable,
      isClearable,
      autoFocus,
      onChange,
      styles,
    };
    /* eslint-disable jsx-a11y/label-has-for */
    return (
      <div className={classNames.join(' ')}>
        {labelText && (
          <label className="label" htmlFor={name} id={`${name}-label`}>
            {labelText}
            {isRequired && <span className="required">*</span>}
          </label>
        )}
        {hintText && (
          <span id={`${name}-hint`} className="form-element-hint">
            {hintText}
          </span>
        )}
        {errorText && <span className="form-element-error-msg">{errorText}</span>}
        <Select {...singleProps} />
      </div>
    );
    /* eslint-enable */
  }
}

export default ReactSelect;
