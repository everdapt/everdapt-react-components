import React from 'react';
/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { text, number } from '@storybook/addon-knobs';
import { InputPin, CheckboxGroup, InputPasswordStrength } from '../src/components';

storiesOf('Inputs/InputPin', module)
  .add(
    'Configurable length',
    () => (
      <InputPin
        id="pin"
        length={number('Length', 5)}
        labelText={text('Label', 'Enter the code we sent to XXXXXX4578')}
      />
    ),
    {
      info: `
    # InputPin
    A pin input type
  `,
    },
  )
  .add('Do not focus by default', () => (
    <InputPin id="pin" length={6} labelText={text('Label', 'Enter pin')} focusOnLoad={false} />
  ))
  .add('With an error', () => (
    <InputPin id="pin" length={7} labelText={text('Label', 'Enter pin')} errorText="Whoops!" />
  ));

const labelText = `Please check if you have any of the following:`;
const list = [
  {
    label: 'Cardiac Pacemaker',
    value: 'Cardiac Pacemaker',
  },
  {
    label: 'Implanted Cardiac Defibrillator, Cardiac Electrodes, Pacing Wires, Internal Electrodes',
    value: 'Implanted Cardiac Defibrillator, Cardiac Electrodes, Pacing Wires, Internal Electrodes',
  },
  {
    label: 'Aneurysm Clip(s)',
    value: 'Aneurysm Clip(s)',
  },
  {
    label: 'Hearing Aid',
    value: 'Hearing Aid',
  },
  {
    label: 'Soft Tissue Expander (e.g. breast)',
    value: 'Soft Tissue Expander (e.g. breast)',
  },
  {
    label: 'Swan-Ganz Catheter',
    value: 'Swan-Ganz Catheter',
  },
  {
    label: 'Worked with Metal OR Metal Fragments in Eyes',
    value: 'Worked with Metal OR Metal Fragments in Eyes',
  },
  {
    label: 'Eye Prosthesis or device such as eyelid spring, wire, or implant',
    value: 'Eye Prosthesis or device such as eyelid spring, wire, or implant',
  },
  {
    label: 'Metallic Fragment or Foreign Body',
    value: 'Metallic Fragment or Foreign Body',
  },
  {
    label: 'Heart Valve Prosthesis',
    value: 'Heart Valve Prosthesis',
  },
  {
    label: 'Electronic Implant or Device',
    value: 'Electronic Implant or Device',
  },
  {
    label: 'Neurostimulation or Spinal Cord Stimulator',
    value: 'Neurostimulation or Spinal Cord Stimulator',
  },
  {
    label: 'Bone Growth Stimulator',
    value: 'Bone Growth Stimulator',
  },
  {
    label: 'Magnetically Activated Implant or Device',
    value: 'Magnetically Activated Implant or Device',
  },
  {
    label: 'Magnetically Activated Implant or Device',
    value: 'Magnetically Activated Implant or Device',
  },
  {
    label: 'Magnetic Stent, Coil, or Filter',
    value: 'Magnetic Stent, Coil, or Filter',
  },
  {
    label: 'Wire Mesh Implant',
    value: 'Wire Mesh Implant',
  },
  {
    label: 'Surgical Staples, Clips, or Metallic Sutures',
    value: 'Surgical Staples, Clips, or Metallic Sutures',
  },
  {
    label: 'Any Other Prosthesis (Limb, Penile Implant, Eye)',
    value: 'Any Other Prosthesis (Limb, Penile Implant, Eye)',
  },
  {
    label: 'Joint Replacement',
    value: 'Joint Replacement',
  },
  {
    label: 'Bone/Joint Pin or Screw, Nail, Wire, or Plate',
    value: 'Bone/Joint Pin or Screw, Nail, Wire, or Plate',
  },
  {
    label: 'IUD, Diaphragm, or Pessary',
    value: 'IUD, Diaphragm, or Pessary',
  },
  {
    label: 'Medicine Patches',
    value: 'Medicine Patches',
  },
  {
    label: 'SHUNT(S) - spinal/intraventricular/peritoneal',
    value: 'SHUNT(S) - spinal/intraventricular/peritoneal',
  },
  {
    label: 'Infusion Pump such as Insulin Pump or Other Drug Infusion Device',
    value: 'Infusion Pump such as Insulin Pump or Other Drug Infusion Device',
  },
  {
    label: 'Dentures, Magnetic Dental Implants, or Hearing Aids',
    value: 'Dentures, Magnetic Dental Implants, or Hearing Aids',
  },
  {
    label: 'Body Piercing Jewelry',
    value: 'Body Piercing Jewelry',
  },
  {
    label: 'Tattoos or Permanent Makeup',
    value: 'Tattoos or Permanent Makeup',
  },
  {
    label: 'Any Other Device or Object in Body',
    value: 'Any Other Device or Object in Body',
  },
];

const onChangeCheckboxGroup = values => {};

const values = ['Tattoos or Permanent Makeup'];
const fieldset = {
  id: 'CHECKBOX',
  list,
  labelText,
  values,
  onChange: onChangeCheckboxGroup,
  isRequired: true,
  noneOfAbove: true,
};

storiesOf('Inputs/ChechboxGroup', module).add('Test', () => <CheckboxGroup {...fieldset} />);

storiesOf('Inputs/Password', module).add('Test', () => (
  <InputPasswordStrength id="InputPasswordStrength" labelText="Pick a password" />
));
