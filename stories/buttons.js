import React from 'react';
/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
/* eslint-enable */
import { Button, InputPin } from '../index';
import styles from './css/styles.css';

storiesOf('Buttons', module).add('Standard button', () => <Button className="button">Standard Button</Button>);
