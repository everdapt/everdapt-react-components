import resolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import external from 'rollup-plugin-peer-deps-external';
import progress from 'rollup-plugin-progress';
import filesize from 'rollup-plugin-filesize';
import babel from 'rollup-plugin-babel';
import pkg from './package.json';

export default [
  {
    input: 'index.js',
    output: [
      {
        file: pkg.main,
        format: 'cjs',
      },
      {
        file: pkg.module,
        format: 'esm',
      },
    ],
    external: [
      'react',
      'react-dom',
      'react-router-dom',
      'prop-types',
      'react-select',
      'react-date-picker',
      'luxon',
      'zxcvbn-async',
    ],
    plugins: [
      replace({
        'process.env.NODE_ENV': JSON.stringify('production'),
      }),
      progress(),
      filesize(),
      external(),
      resolve({
        extensions: ['.js', '.jsx'],
        customResolveOptions: {
          moduleDirectory: 'node_modules',
        },
      }),
      babel({
        presets: [['@babel/preset-env', { modules: false }], '@babel/preset-react'],
        babelrc: false,
        exclude: 'node_modules/**',
      }),
    ],
  },
];
