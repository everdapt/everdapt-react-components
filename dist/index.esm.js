import React, { PureComponent, Component } from 'react';
import PropTypes from 'prop-types';
import zxcvbnAsync from 'zxcvbn-async';
import DatePicker from 'react-date-picker/dist/entry.nostyle';
import { DateTime } from 'luxon';
import Select from 'react-select';

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

/* eslint-disable react/button-has-type */

var Button =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Button, _PureComponent);

  function Button() {
    _classCallCheck(this, Button);

    return _possibleConstructorReturn(this, _getPrototypeOf(Button).apply(this, arguments));
  }

  _createClass(Button, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          onClick = _this$props.onClick,
          className = _this$props.className,
          children = _this$props.children,
          btnType = _this$props.btnType,
          isDisabled = _this$props.isDisabled;
      return React.createElement("button", {
        className: className,
        onClick: onClick,
        type: btnType,
        disabled: isDisabled
      }, children);
    }
  }]);

  return Button;
}(PureComponent);
/* eslint-enable */


_defineProperty(Button, "propTypes", {
  onClick: PropTypes.func,
  className: PropTypes.string,
  btnType: PropTypes.string,
  isDisabled: PropTypes.bool,
  children: PropTypes.node.isRequired
});

_defineProperty(Button, "defaultProps", {
  onClick: null,
  isDisabled: false,
  btnType: 'button',
  className: 'btn'
});

var Modal =
/*#__PURE__*/
function (_Component) {
  _inherits(Modal, _Component);

  function Modal(props) {
    var _this;

    _classCallCheck(this, Modal);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Modal).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "closeModal", function () {
      _this.setState({
        isShowing: false
      });
    });

    _this.state = {
      isShowing: false
    };
    return _this;
  }

  _createClass(Modal, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          isShowing = _this$props.isShowing,
          onOpen = _this$props.onOpen,
          onClose = _this$props.onClose;
      isShowing ? onOpen() : onClose();
      this.setState({
        isShowing: isShowing
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          modalTitle = _this$props2.modalTitle,
          children = _this$props2.children;
      var isShowing = this.state.isShowing;
      var classNames = ['modal'];

      if (isShowing) {
        classNames.push('modal--open');
        document.body.classList.add('has-modal');
      } else {
        document.body.classList.remove('has-modal');
      }

      return React.createElement("div", {
        className: "".concat(classNames.join(' '))
      }, React.createElement("div", {
        className: "modal-content"
      }, React.createElement("button", {
        className: "modal-close-icon modal-close",
        type: "button",
        onClick: this.closeModal
      }), React.createElement("div", {
        className: "modal-inner"
      }, React.createElement("div", {
        className: "modal-header"
      }, React.createElement("h2", null, modalTitle)), children)));
    }
  }]);

  return Modal;
}(Component);

_defineProperty(Modal, "propTypes", {
  isShowing: PropTypes.bool.isRequired,
  modalTitle: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  onOpen: PropTypes.func,
  onClose: PropTypes.func
});

_defineProperty(Modal, "defaultProps", {
  onOpen: function onOpen() {},
  onClose: function onClose() {}
});

var SubmitButton =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(SubmitButton, _PureComponent);

  function SubmitButton() {
    _classCallCheck(this, SubmitButton);

    return _possibleConstructorReturn(this, _getPrototypeOf(SubmitButton).apply(this, arguments));
  }

  _createClass(SubmitButton, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          isDisabled = _this$props.isDisabled,
          btnText = _this$props.btnText,
          className = _this$props.className,
          btnClassName = _this$props.btnClassName,
          props = _objectWithoutProperties(_this$props, ["isDisabled", "btnText", "className", "btnClassName"]);

      return React.createElement("div", {
        className: className
      }, React.createElement(Button, _extends({}, props, {
        btnType: "submit",
        isDisabled: isDisabled,
        className: btnClassName
      }), btnText));
    }
  }]);

  return SubmitButton;
}(PureComponent);

_defineProperty(SubmitButton, "propTypes", {
  btnText: PropTypes.string.isRequired,
  className: PropTypes.string,
  isDisabled: PropTypes.bool,
  btnClassName: PropTypes.string
});

_defineProperty(SubmitButton, "defaultProps", {
  isDisabled: false,
  className: 'form-element form-element--submit',
  btnClassName: 'btn btn-action'
});

var inputTypeMapping = {
  text: 'text',
  email: 'email',
  search: 'text',
  tel: 'tel'
};

var InputField =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputField, _PureComponent);

  function InputField() {
    _classCallCheck(this, InputField);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputField).apply(this, arguments));
  }

  _createClass(InputField, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          type = _this$props.type,
          placeholder = _this$props.placeholder,
          labelText = _this$props.labelText,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          value = _this$props.value,
          isRequired = _this$props.isRequired,
          isDisabled = _this$props.isDisabled,
          hasGlobalError = _this$props.hasGlobalError,
          onChange = _this$props.onChange,
          pattern = _this$props.pattern;
      /* eslint-disable jsx-a11y/label-has-for */

      var classNames = ['form-element', "form-element--".concat(type)];
      if (hasGlobalError || errorText) classNames.push('form-element--error');
      if (isRequired) classNames.push('form-element--required');

      var inputProps = _objectSpread2({
        id: id,
        name: id,
        type: inputTypeMapping[type] || 'text',
        required: isRequired,
        disabled: isDisabled,
        onChange: onChange,
        placeholder: placeholder || undefined,
        value: value,
        pattern: pattern || undefined
      }, hintText && {
        'aria-describedby': "".concat(id, "-hint")
      });

      return React.createElement("div", {
        className: "".concat(classNames.join(' '))
      }, labelText && React.createElement("label", {
        className: "label",
        htmlFor: id,
        id: "".concat(id, "-label")
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(id, "-hint"),
        className: "form-element-hint"
      }, hintText), errorText && React.createElement("span", {
        className: "form-element-error-msg"
      }, errorText), React.createElement("input", inputProps));
      /* eslint-enable */
    }
  }]);

  return InputField;
}(PureComponent);

_defineProperty(InputField, "propTypes", {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  labelText: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  value: PropTypes.string,
  pattern: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  hasGlobalError: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputField, "defaultProps", {
  placeholder: null,
  labelText: null,
  hintText: null,
  errorText: null,
  value: undefined,
  pattern: null,
  isRequired: false,
  isDisabled: false,
  hasGlobalError: false,
  onChange: null
});

var InputText =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputText, _PureComponent);

  function InputText() {
    _classCallCheck(this, InputText);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputText).apply(this, arguments));
  }

  _createClass(InputText, [{
    key: "render",
    value: function render() {
      return React.createElement(InputField, _extends({}, this.props, {
        type: "text"
      }));
    }
  }]);

  return InputText;
}(PureComponent);

_defineProperty(InputText, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputText, "defaultProps", {
  hintText: null,
  placeholder: null,
  value: undefined,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  onChange: null
});

var InputCheckbox =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputCheckbox, _PureComponent);

  function InputCheckbox() {
    _classCallCheck(this, InputCheckbox);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputCheckbox).apply(this, arguments));
  }

  _createClass(InputCheckbox, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          labelClassName = _this$props.labelClassName,
          labelText = _this$props.labelText,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          isChecked = _this$props.isChecked,
          isDisabled = _this$props.isDisabled,
          isRequired = _this$props.isRequired,
          onChange = _this$props.onChange;
      var classNames = ['form-element', 'form-element--checkbox'];
      if (isDisabled) classNames.push('form-element--disabled');
      if (isRequired) classNames.push('form-element--required');
      if (errorText) classNames.push('form-element--error');

      var inputProps = _objectSpread2({
        id: id,
        name: id,
        type: 'checkbox',
        checked: isChecked,
        disabled: isDisabled,
        required: isRequired,
        onChange: onChange
      }, hintText && {
        'aria-describedby': "".concat(id, "-hint")
      });

      return (
        /* eslint-disable jsx-a11y/label-has-for */
        React.createElement("div", {
          className: classNames.join(' ')
        }, React.createElement("input", inputProps), labelText && React.createElement("label", {
          className: labelClassName,
          htmlFor: id,
          id: "".concat(id, "-label")
        }, labelText, isRequired && React.createElement("span", {
          className: "required"
        }, "*")), hintText && React.createElement("span", {
          id: "".concat(id, "-hint"),
          className: "form-element-hint"
        }, hintText), errorText && React.createElement("span", {
          className: "form-element-error-msg"
        }, errorText))
        /* eslint-enable */

      );
    }
  }]);

  return InputCheckbox;
}(PureComponent);

_defineProperty(InputCheckbox, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isChecked: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isRequired: PropTypes.bool,
  onChange: PropTypes.func,
  labelClassName: PropTypes.string
});

_defineProperty(InputCheckbox, "defaultProps", {
  onChange: null,
  labelClassName: 'label',
  hintText: '',
  errorText: '',
  isChecked: false,
  isDisabled: false,
  isRequired: false
});

var InputEmail =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputEmail, _PureComponent);

  function InputEmail() {
    _classCallCheck(this, InputEmail);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputEmail).apply(this, arguments));
  }

  _createClass(InputEmail, [{
    key: "render",
    value: function render() {
      return React.createElement(InputField, _extends({}, this.props, {
        type: "email"
      }));
    }
  }]);

  return InputEmail;
}(PureComponent);

_defineProperty(InputEmail, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputEmail, "defaultProps", {
  hintText: null,
  placeholder: null,
  value: undefined,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  onChange: null
});

var InputTel =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputTel, _PureComponent);

  function InputTel() {
    _classCallCheck(this, InputTel);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputTel).apply(this, arguments));
  }

  _createClass(InputTel, [{
    key: "render",
    value: function render() {
      return React.createElement(InputField, _extends({}, this.props, {
        type: "tel"
      }));
    }
  }]);

  return InputTel;
}(PureComponent);

_defineProperty(InputTel, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  pattern: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputTel, "defaultProps", {
  hintText: null,
  placeholder: null,
  value: undefined,
  pattern: '[0-9]{10}',
  errorText: null,
  isRequired: false,
  isDisabled: false,
  onChange: null
});

var InputSearch =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputSearch, _PureComponent);

  function InputSearch() {
    _classCallCheck(this, InputSearch);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputSearch).apply(this, arguments));
  }

  _createClass(InputSearch, [{
    key: "render",
    value: function render() {
      return React.createElement(InputField, _extends({}, this.props, {
        type: "search"
      }));
    }
  }]);

  return InputSearch;
}(PureComponent);

_defineProperty(InputSearch, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputSearch, "defaultProps", {
  labelText: null,
  hintText: null,
  placeholder: null,
  value: undefined,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  onChange: null
});

var InputSelect =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputSelect, _PureComponent);

  function InputSelect() {
    _classCallCheck(this, InputSelect);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputSelect).apply(this, arguments));
  }

  _createClass(InputSelect, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          currentValue = _this$props.currentValue,
          options = _this$props.options,
          labelText = _this$props.labelText,
          isButton = _this$props.isButton,
          isDisabled = _this$props.isDisabled,
          isRequired = _this$props.isRequired,
          onChange = _this$props.onChange;
      var classNames = ['form-element', 'form-element--select'];
      if (isButton) classNames.push('button');
      var opts = options.map(function (it) {
        return React.createElement("option", {
          value: it.value,
          key: it.value
        }, it.label);
      });
      /* eslint-disable jsx-a11y/label-has-for */

      return React.createElement("div", {
        className: classNames.join(' ')
      }, React.createElement("label", {
        className: "label",
        htmlFor: id
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), React.createElement("select", {
        id: id,
        name: id,
        onChange: onChange,
        value: currentValue,
        disabled: isDisabled
      }, opts));
      /* eslint-enable */
    }
  }]);

  return InputSelect;
}(PureComponent);

_defineProperty(InputSelect, "propTypes", {
  id: PropTypes.string.isRequired,
  currentValue: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired
  })).isRequired,
  labelText: PropTypes.string,
  isButton: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isRequired: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputSelect, "defaultProps", {
  labelText: null,
  isButton: false,
  isDisabled: false,
  isRequired: false,
  onChange: null
});

var InputTextarea =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputTextarea, _PureComponent);

  function InputTextarea() {
    _classCallCheck(this, InputTextarea);

    return _possibleConstructorReturn(this, _getPrototypeOf(InputTextarea).apply(this, arguments));
  }

  _createClass(InputTextarea, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          labelText = _this$props.labelText,
          hintText = _this$props.hintText,
          value = _this$props.value,
          isDisabled = _this$props.isDisabled,
          isRequired = _this$props.isRequired,
          errorText = _this$props.errorText,
          onChange = _this$props.onChange;
      var classNames = ['form-element', "form-element--textarea"].concat(errorText ? ['form-element--error'] : []).concat(isRequired ? ['form-element--required'] : []);

      var inputProps = _objectSpread2({
        id: id,
        name: id,
        disabled: isDisabled,
        required: isRequired,
        onChange: onChange,
        value: value
      }, hintText && {
        'aria-describedby': "".concat(id, "-hint")
      });
      /* eslint-disable jsx-a11y/label-has-for */


      return React.createElement("div", {
        className: classNames.join(' ')
      }, labelText && React.createElement("label", {
        className: "label",
        htmlFor: id,
        id: "".concat(id, "-label")
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(id, "-hint"),
        className: "form-element-hint"
      }, hintText), errorText && React.createElement("span", {
        className: "form-element-error-msg"
      }, errorText), React.createElement("textarea", inputProps));
      /* eslint-enable */
    }
  }]);

  return InputTextarea;
}(PureComponent);

_defineProperty(InputTextarea, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string,
  value: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isDisabled: PropTypes.bool,
  isRequired: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(InputTextarea, "defaultProps", {
  labelText: null,
  hintText: null,
  errorText: null,
  value: undefined,
  isDisabled: false,
  isRequired: false,
  onChange: null
});

var BUTTON_TEXT_SHOW = 'Show';
var BUTTON_TEXT_HIDE = 'Hide';

var InputPassword =
/*#__PURE__*/
function (_Component) {
  _inherits(InputPassword, _Component);

  function InputPassword() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, InputPassword);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(InputPassword)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isShowingPassword: _this.props.showByDefault,
      btnText: BUTTON_TEXT_SHOW
    });

    _defineProperty(_assertThisInitialized(_this), "onShowButtonClick", function () {
      _this.setState(function (prevState) {
        var isShowingPassword = prevState.isShowingPassword;
        return {
          isShowingPassword: !isShowingPassword,
          btnText: !isShowingPassword ? BUTTON_TEXT_HIDE : BUTTON_TEXT_SHOW
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (e) {
      var onChange = _this.props.onChange;

      if (onChange) {
        onChange(e);
      }
    });

    return _this;
  }

  _createClass(InputPassword, [{
    key: "render",

    /* eslint-disable jsx-a11y/label-has-for */
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          placeholder = _this$props.placeholder,
          labelText = _this$props.labelText,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          isRequired = _this$props.isRequired,
          isDisabled = _this$props.isDisabled,
          hasGlobalError = _this$props.hasGlobalError,
          shouldCheckStrength = _this$props.shouldCheckStrength,
          strengthState = _this$props.strengthState,
          strengthClass = _this$props.strengthClass;
      var _this$state = this.state,
          isShowingPassword = _this$state.isShowingPassword,
          btnText = _this$state.btnText;
      var classNames = ['form-element', 'form-element--password'];
      if (hasGlobalError) classNames.push('form-element--error');
      if (isRequired) classNames.push('form-element--required');
      if (shouldCheckStrength) classNames.push('strength-bar');
      if (strengthClass) classNames.push(strengthClass);

      var inputProps = _objectSpread2({
        id: id,
        name: id,
        type: isShowingPassword ? 'text' : 'password',
        required: isRequired,
        disabled: isDisabled,
        onChange: this.handleChange,
        placeholder: placeholder || undefined
      }, hintText && {
        'aria-describedby': "".concat(id, "-hint")
      });

      return React.createElement("div", {
        className: classNames.join(' ')
      }, labelText && React.createElement("label", {
        className: "label",
        htmlFor: id,
        id: "".concat(id, "-label")
      }, labelText), hintText && React.createElement("span", {
        id: "".concat(id, "-hint"),
        className: "form-element-hint"
      }, hintText), errorText && React.createElement("span", {
        className: "form-element-error-msg"
      }, errorText), React.createElement("button", {
        className: "show-password",
        type: "button",
        onClick: this.onShowButtonClick
      }, btnText), React.createElement("input", inputProps), shouldCheckStrength && React.createElement("span", {
        className: "password-strength-state"
      }, strengthState));
    }
    /* eslint-enable */

  }]);

  return InputPassword;
}(Component);

_defineProperty(InputPassword, "propTypes", {
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  labelText: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  hasGlobalError: PropTypes.bool,
  showByDefault: PropTypes.bool,
  shouldCheckStrength: PropTypes.bool,
  strengthState: PropTypes.string,
  strengthClass: PropTypes.string,
  onChange: PropTypes.func
});

_defineProperty(InputPassword, "defaultProps", {
  placeholder: null,
  labelText: null,
  hintText: null,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  showByDefault: false,
  shouldCheckStrength: false,
  strengthState: null,
  strengthClass: null,
  onChange: null,
  hasGlobalError: false
});

var zxcvbnPromose = zxcvbnAsync.load({
  sync: false
});

var isTooShort = function isTooShort(password, minLength) {
  return password.length < minLength;
};

var scoreWords = ['Weak', 'Okay', 'Good', 'Strong', 'Great'];
var tooShortWord = 'Too short';
var userInputs = [];
var hint = 'Suggest at least 1 or 2 uppercase letters and/or numbers.';

var InputPasswordStrength =
/*#__PURE__*/
function (_Component) {
  _inherits(InputPasswordStrength, _Component);

  function InputPasswordStrength() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, InputPasswordStrength);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(InputPasswordStrength)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      score: 0,
      isValid: false,
      password: ''
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (e) {
      var password = e.target.value;
      var _this$props = _this.props,
          changeCallback = _this$props.changeCallback,
          minScore = _this$props.minScore,
          minLength = _this$props.minLength;
      var score = 0;

      if (!isTooShort(password, minLength)) {
        zxcvbnPromose.then(function (zxcvbn) {
          var result = zxcvbn(password, userInputs);
          score = result.score === -1 ? 0 : result.score;
        });
      }

      _this.setState({
        isValid: score >= minScore,
        password: password,
        score: score
      }, function () {
        if (changeCallback) {
          changeCallback(_this.state);
        }
      });
    });

    return _this;
  }

  _createClass(InputPasswordStrength, [{
    key: "render",
    value: function render() {
      var _this$state = this.state,
          score = _this$state.score,
          password = _this$state.password,
          isValid = _this$state.isValid;

      var _this$props2 = this.props,
          minScore = _this$props2.minScore,
          minLength = _this$props2.minLength,
          changeCallback = _this$props2.changeCallback,
          hintText = _this$props2.hintText,
          rest = _objectWithoutProperties(_this$props2, ["minScore", "minLength", "changeCallback", "hintText"]);

      var strengthDesc = isTooShort(password, minLength) ? tooShortWord : scoreWords[score];
      /* eslint-disable no-nested-ternary */

      var strengthClass = isValid ? 'is-password-valid' : password.length ? 'is-password-invalid' : null;
      /* eslint-enable */

      return React.createElement(InputPassword, _extends({}, rest, {
        hintText: "Must be ".concat(minLength, " characters minimum. ").concat(hintText),
        shouldCheckStrength: true,
        strengthState: strengthDesc,
        strengthClass: strengthClass,
        onChange: this.handleChange
      }));
    }
  }]);

  return InputPasswordStrength;
}(Component);

_defineProperty(InputPasswordStrength, "propTypes", {
  id: PropTypes.string.isRequired,
  hintText: PropTypes.string,
  labelText: PropTypes.string,
  placeholder: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  showByDefault: PropTypes.bool,
  hasGlobalError: PropTypes.bool,
  changeCallback: PropTypes.func,
  minLength: PropTypes.number,
  minScore: PropTypes.number
});

_defineProperty(InputPasswordStrength, "defaultProps", {
  placeholder: null,
  hasGlobalError: false,
  changeCallback: null,
  minLength: 8,
  minScore: 2,
  hintText: hint,
  labelText: null,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  showByDefault: false
});

var ERROR_TEXT = 'Invalid number.';

var InputPin =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(InputPin, _PureComponent);

  function InputPin(props) {
    var _this;

    _classCallCheck(this, InputPin);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(InputPin).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "setRef", function (ref) {
      _this.inputRefs.push(ref);
    });

    _defineProperty(_assertThisInitialized(_this), "handleOnKeyDown", function (index, e) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          length = _this$props.length;
      var value = e.target.value;

      if (e.keyCode === 8) {
        if (value === '' && index) {
          _this.setState(function (prevState) {
            return {
              pinValue: prevState.pinValue.substring(0, index - 1),
              error: ''
            };
          }, function () {
            _this.inputRefs[index - 1].focus();
          });
        }

        return;
      }

      if (index !== length - 1) {
        setTimeout(function () {
          _this.inputRefs[index + 1].focus();
        }, 0);
      } else {
        setTimeout(function () {
          _this.inputRefs[index].blur();
        }, 0);
      }

      onChange();
    });

    _defineProperty(_assertThisInitialized(_this), "handleOnBlur", function (index, e) {
      var pattern = /[0-9]/g;
      var value = e.target.value;

      if (value !== '' && !pattern.test(value)) {
        _this.setState({
          error: ERROR_TEXT
        }, function () {
          _this.inputRefs[index].focus();

          _this.inputRefs[index].select();
        });
      } else if (value !== '') {
        _this.setState(function (prevState) {
          return {
            pinValue: prevState.pinValue.concat(value),
            error: ''
          };
        }, function () {
          var getValue = _this.props.getValue;
          var pinValue = _this.state.pinValue;
          getValue(pinValue);
        });
      }
    });

    _this.inputRefs = [];
    _this.state = {
      error: '',
      pinValue: ''
    };
    return _this;
  }

  _createClass(InputPin, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props2 = this.props,
          focusOnLoad = _this$props2.focusOnLoad,
          errorText = _this$props2.errorText;
      this.setState({
        error: errorText
      });

      if (focusOnLoad && this.inputRefs[0]) {
        this.inputRefs[0].focus();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var classNames = ['form-group', 'form-group--pin'];
      var _this$props3 = this.props,
          id = _this$props3.id,
          length = _this$props3.length,
          labelText = _this$props3.labelText,
          hintText = _this$props3.hintText,
          isDisabled = _this$props3.isDisabled,
          isRequired = _this$props3.isRequired;
      var error = this.state.error;
      var inputList = [];
      var inputProps = {};
      if (isDisabled) classNames.push('form-group--disabled');
      if (isRequired) classNames.push('form-group--required');
      if (error) classNames.push('form-group--error');
      /* eslint-disable jsx-a11y/label-has-for */

      /* eslint-disable jsx-a11y/label-has-associated-control */

      var _loop = function _loop(i) {
        inputProps = {
          id: "pin_".concat(i + 1),
          key: "pin_".concat(i + 1),
          name: 'pin',
          maxLength: 1,
          type: 'text',
          ref: _this2.setRef,
          onBlur: function onBlur(e) {
            return _this2.handleOnBlur(i, e);
          },
          onKeyDown: function onKeyDown(e) {
            return _this2.handleOnKeyDown(i, e);
          }
        };
        inputList.push(React.createElement("input", inputProps));
      };

      for (var i = 0; i < length; i++) {
        _loop(i);
      }

      return React.createElement("fieldset", {
        className: classNames.join(' ')
      }, labelText && React.createElement("legend", {
        className: "label"
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(id, "-hint"),
        className: "form-group-hint"
      }, hintText), error && React.createElement("span", {
        className: "form-group--error-msg"
      }, error), React.createElement("div", {
        className: "input-group"
      }, " ", inputList, " "));
    }
  }]);

  return InputPin;
}(PureComponent);

_defineProperty(InputPin, "propTypes", {
  id: PropTypes.string.isRequired,
  length: PropTypes.number,
  hintText: PropTypes.string,
  labelText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  focusOnLoad: PropTypes.bool,
  onChange: PropTypes.func,
  getValue: PropTypes.func
});

_defineProperty(InputPin, "defaultProps", {
  length: 6,
  labelText: null,
  hintText: null,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  focusOnLoad: true,
  onChange: function onChange() {},
  getValue: function getValue() {}
});

var Toggle =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(Toggle, _PureComponent);

  function Toggle() {
    _classCallCheck(this, Toggle);

    return _possibleConstructorReturn(this, _getPrototypeOf(Toggle).apply(this, arguments));
  }

  _createClass(Toggle, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          className = _this$props.className,
          labelClassName = _this$props.labelClassName,
          labelText = _this$props.labelText,
          isChecked = _this$props.isChecked,
          isDisabled = _this$props.isDisabled,
          isRequired = _this$props.isRequired,
          isInline = _this$props.isInline,
          onChange = _this$props.onChange;
      var wrapperClass = !isInline ? className : "".concat(className, "-inline");
      return (
        /* eslint-disable jsx-a11y/label-has-for */
        React.createElement("div", {
          className: wrapperClass
        }, React.createElement("label", {
          className: labelClassName,
          htmlFor: id
        }, labelText, isRequired && React.createElement("span", {
          className: "required"
        }, "*"), isRequired && React.createElement("span", {
          className: "required"
        }, "*")), React.createElement("input", {
          id: id,
          name: id,
          type: "checkbox",
          onChange: onChange,
          checked: isChecked,
          disabled: isDisabled
        }), React.createElement("span", {
          className: "toggle"
        }))
        /* eslint-enable */

      );
    }
  }]);

  return Toggle;
}(PureComponent);

_defineProperty(Toggle, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  isChecked: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool,
  isRequired: PropTypes.bool,
  isInline: PropTypes.bool,
  onChange: PropTypes.func,
  className: PropTypes.string,
  labelClassName: PropTypes.string
});

_defineProperty(Toggle, "defaultProps", {
  onChange: null,
  className: 'form-element form-element--toggle',
  labelClassName: 'label',
  isDisabled: false,
  isRequired: false,
  isInline: false
});

var ReactDatePicker =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(ReactDatePicker, _PureComponent);

  function ReactDatePicker() {
    _classCallCheck(this, ReactDatePicker);

    return _possibleConstructorReturn(this, _getPrototypeOf(ReactDatePicker).apply(this, arguments));
  }

  _createClass(ReactDatePicker, [{
    key: "render",

    /* eslint-disable react/forbid-prop-types */

    /* eslint-enable */
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          labelText = _this$props.labelText,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          value = _this$props.value,
          isRequired = _this$props.isRequired,
          isDisabled = _this$props.isDisabled,
          minDate = _this$props.minDate,
          maxDate = _this$props.maxDate,
          locale = _this$props.locale,
          hasGlobalError = _this$props.hasGlobalError,
          onChange = _this$props.onChange;
      /* eslint-disable jsx-a11y/label-has-for */

      var classNames = ['form-element', 'form-element--datepicker'];
      if (hasGlobalError || errorText) classNames.push('form-element--error');
      if (isRequired) classNames.push('form-element--required');
      var dateProps = {
        id: id,
        name: id,
        required: isRequired,
        disabled: isDisabled,
        minDate: minDate,
        maxDate: maxDate,
        locale: locale,
        onChange: onChange,
        value: value
      };
      return React.createElement("div", {
        className: "".concat(classNames.join(' '))
      }, labelText && React.createElement("label", {
        className: "label",
        htmlFor: id,
        id: "".concat(id, "-label")
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(id, "-hint"),
        className: "form-element-hint"
      }, hintText), errorText && React.createElement("span", {
        className: "form-element-error-msg"
      }, errorText), React.createElement(DatePicker, dateProps));
      /* eslint-enable */
    }
  }]);

  return ReactDatePicker;
}(PureComponent);

_defineProperty(ReactDatePicker, "propTypes", {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  value: PropTypes.object,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  minDate: PropTypes.instanceOf(Date),
  maxDate: PropTypes.instanceOf(Date),
  locale: PropTypes.string,
  hasGlobalError: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(ReactDatePicker, "defaultProps", {
  labelText: null,
  hintText: null,
  errorText: null,
  value: null,
  isRequired: false,
  isDisabled: false,
  minDate: DateTime.fromJSDate(new Date()).minus({
    years: 100
  }).toJSDate(),
  maxDate: DateTime.fromJSDate(new Date()).minus({
    days: 1
  }).toJSDate(),
  locale: 'en-US',
  hasGlobalError: false,
  onChange: null
});

var styles = {
  clearIndicator: function clearIndicator() {
    return {};
  },
  dropdownIndicator: function dropdownIndicator() {
    return {};
  },
  container: function container() {
    return {};
  },
  control: function control() {
    return {};
  },
  group: function group() {
    return {};
  },
  groupHeading: function groupHeading() {
    return {};
  },
  indicatorsContainer: function indicatorsContainer() {
    return {};
  },
  indicatorSeparator: function indicatorSeparator() {
    return {};
  },
  input: function input() {
    return {};
  },
  loadingIndicator: function loadingIndicator() {
    return {};
  },
  loadingMessage: function loadingMessage() {
    return {};
  },
  menu: function menu() {
    return {};
  },
  menuList: function menuList() {
    return {};
  },
  menuPortal: function menuPortal() {
    return {};
  },
  multiValue: function multiValue() {
    return {};
  },
  multiValueLabel: function multiValueLabel() {
    return {};
  },
  multiValueRemove: function multiValueRemove() {
    return {};
  },
  noOptionsMessage: function noOptionsMessage() {
    return {};
  },
  option: function option() {
    return {};
  },
  placeholder: function placeholder() {
    return {};
  },
  singleValue: function singleValue() {
    return {};
  },
  valueContainer: function valueContainer() {
    return {};
  }
};

var ReactSelect =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(ReactSelect, _PureComponent);

  function ReactSelect() {
    _classCallCheck(this, ReactSelect);

    return _possibleConstructorReturn(this, _getPrototypeOf(ReactSelect).apply(this, arguments));
  }

  _createClass(ReactSelect, [{
    key: "render",

    /* eslint-disable react/forbid-prop-types */

    /* eslint-enable */
    value: function render() {
      var _this$props = this.props,
          name = _this$props.name,
          options = _this$props.options,
          value = _this$props.value,
          labelText = _this$props.labelText,
          placeholder = _this$props.placeholder,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          isRequired = _this$props.isRequired,
          isDisabled = _this$props.isDisabled,
          isSearchable = _this$props.isSearchable,
          isClearable = _this$props.isClearable,
          autoFocus = _this$props.autoFocus,
          onChange = _this$props.onChange;
      var classNames = ['form-element', 'form-element--singleselect'];
      if (errorText) classNames.push('form-element--error');
      if (isRequired) classNames.push('form-element--required');
      var singleProps = {
        name: name,
        options: options,
        value: value,
        classNamePrefix: 'react-select',
        className: 'input--react-select',
        placeholder: placeholder,
        isDisabled: isDisabled,
        isSearchable: isSearchable,
        isClearable: isClearable,
        autoFocus: autoFocus,
        onChange: onChange,
        styles: styles
      };
      /* eslint-disable jsx-a11y/label-has-for */

      return React.createElement("div", {
        className: classNames.join(' ')
      }, labelText && React.createElement("label", {
        className: "label",
        htmlFor: name,
        id: "".concat(name, "-label")
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(name, "-hint"),
        className: "form-element-hint"
      }, hintText), errorText && React.createElement("span", {
        className: "form-element-error-msg"
      }, errorText), React.createElement(Select, singleProps));
      /* eslint-enable */
    }
  }]);

  return ReactSelect;
}(PureComponent);

_defineProperty(ReactSelect, "propTypes", {
  name: PropTypes.string.isRequired,
  options: PropTypes.array,
  value: PropTypes.object,
  labelText: PropTypes.string,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isSearchable: PropTypes.bool,
  isClearable: PropTypes.bool,
  autoFocus: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(ReactSelect, "defaultProps", {
  options: null,
  value: null,
  labelText: null,
  className: null,
  placeholder: null,
  hintText: null,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  isSearchable: true,
  isClearable: true,
  autoFocus: false,
  onChange: null
});

var styles$1 = {
  clearIndicator: function clearIndicator() {
    return {};
  },
  dropdownIndicator: function dropdownIndicator() {
    return {};
  },
  container: function container() {
    return {};
  },
  control: function control() {
    return {};
  },
  group: function group() {
    return {};
  },
  groupHeading: function groupHeading() {
    return {};
  },
  indicatorsContainer: function indicatorsContainer() {
    return {};
  },
  indicatorSeparator: function indicatorSeparator() {
    return {};
  },
  input: function input() {
    return {};
  },
  loadingIndicator: function loadingIndicator() {
    return {};
  },
  loadingMessage: function loadingMessage() {
    return {};
  },
  menu: function menu() {
    return {};
  },
  menuList: function menuList() {
    return {};
  },
  menuPortal: function menuPortal() {
    return {};
  },
  multiValue: function multiValue() {
    return {};
  },
  multiValueLabel: function multiValueLabel() {
    return {};
  },
  multiValueRemove: function multiValueRemove() {
    return {};
  },
  noOptionsMessage: function noOptionsMessage() {
    return {};
  },
  option: function option() {
    return {};
  },
  placeholder: function placeholder() {
    return {};
  },
  singleValue: function singleValue() {
    return {};
  },
  valueContainer: function valueContainer() {
    return {};
  }
};

var ReactMultiSelect =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(ReactMultiSelect, _PureComponent);

  function ReactMultiSelect() {
    _classCallCheck(this, ReactMultiSelect);

    return _possibleConstructorReturn(this, _getPrototypeOf(ReactMultiSelect).apply(this, arguments));
  }

  _createClass(ReactMultiSelect, [{
    key: "render",

    /* eslint-disable react/forbid-prop-types */

    /* eslint-enable */
    value: function render() {
      var _this$props = this.props,
          name = _this$props.name,
          options = _this$props.options,
          value = _this$props.value,
          minItems = _this$props.minItems,
          maxItems = _this$props.maxItems,
          labelText = _this$props.labelText,
          placeholder = _this$props.placeholder,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          isRequired = _this$props.isRequired,
          isDisabled = _this$props.isDisabled,
          isSearchable = _this$props.isSearchable,
          isClearable = _this$props.isClearable,
          autoFocus = _this$props.autoFocus,
          onChange = _this$props.onChange;
      var required = isRequired;
      var disabled = isDisabled;
      var classNames = ['form-element', 'form-element--multiselect'];
      var currentItems = 0;

      if (value) {
        if (Array.isArray(value)) {
          currentItems = value.length;
        } else {
          currentItems = Object.keys(value).length ? 1 : 0;
        }
      }

      if (currentItems === maxItems) disabled = true;
      if (minItems) required = true;
      if (errorText) classNames.push('form-element--error');
      if (required) classNames.push('form-element--required');
      var multiProps = {
        name: name,
        options: options,
        value: value,
        classNamePrefix: 'react-select',
        className: 'input--react-select--multi',
        placeholder: placeholder,
        isDisabled: disabled,
        isSearchable: isSearchable,
        isClearable: isClearable,
        isMulti: true,
        autoFocus: autoFocus,
        onChange: onChange,
        styles: styles$1
      };
      /* eslint-disable jsx-a11y/label-has-for */

      return React.createElement("div", {
        className: classNames.join(' ')
      }, labelText && React.createElement("label", {
        className: "label",
        htmlFor: name,
        id: "".concat(name, "-label")
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(name, "-hint"),
        className: "form-element-hint"
      }, hintText), errorText && React.createElement("span", {
        className: "form-element-error-msg"
      }, errorText), React.createElement(Select, multiProps));
      /* eslint-enable */
    }
  }]);

  return ReactMultiSelect;
}(PureComponent);

_defineProperty(ReactMultiSelect, "propTypes", {
  name: PropTypes.string.isRequired,
  options: PropTypes.array,
  value: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  minItems: PropTypes.number,
  maxItems: PropTypes.number,
  labelText: PropTypes.string,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isRequired: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isSearchable: PropTypes.bool,
  isClearable: PropTypes.bool,
  autoFocus: PropTypes.bool,
  onChange: PropTypes.func
});

_defineProperty(ReactMultiSelect, "defaultProps", {
  options: null,
  value: null,
  minItems: 0,
  maxItems: 0,
  labelText: null,
  className: null,
  placeholder: null,
  hintText: null,
  errorText: null,
  isRequired: false,
  isDisabled: false,
  isSearchable: true,
  isClearable: true,
  autoFocus: false,
  onChange: null
});

var ERROR_TEXT$1 = 'You have to check at least one item.';

var CheckboxGroup =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(CheckboxGroup, _PureComponent);

  /* eslint-disable react/forbid-prop-types */

  /* eslint-enable */
  function CheckboxGroup(props) {
    var _this;

    _classCallCheck(this, CheckboxGroup);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CheckboxGroup).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleOnChange", function (e) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          noneOfAboveText = _this$props.noneOfAboveText;
      var _e$target = e.target,
          value = _e$target.value,
          checked = _e$target.checked;

      _this.setState(function (prevState) {
        return {
          values: checked ? [].concat(_toConsumableArray(prevState.values.filter(function (val) {
            return val !== noneOfAboveText;
          })), [value]) : prevState.values.filter(function (val) {
            return !prevState.values.includes(val);
          })
        };
      }, function () {
        var values = _this.state.values;
        onChange(values);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "noneOfAboveChange", function (e) {
      var onChange = _this.props.onChange;
      var _e$target2 = e.target,
          value = _e$target2.value,
          checked = _e$target2.checked;

      if (checked) {
        _this.setState(function () {
          return {
            values: [value]
          };
        }, function () {
          var values = _this.state.values;
          onChange(values);
        });
      }
    });

    _this.state = {
      values: []
    };
    return _this;
  }

  _createClass(CheckboxGroup, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var values = this.props.values;
      this.setState({
        values: values
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          id = _this$props2.id,
          list = _this$props2.list,
          labelClassName = _this$props2.labelClassName,
          labelText = _this$props2.labelText,
          hintText = _this$props2.hintText,
          errorText = _this$props2.errorText,
          isDisabled = _this$props2.isDisabled,
          isRequired = _this$props2.isRequired,
          noneOfAbove = _this$props2.noneOfAbove,
          noneOfAboveText = _this$props2.noneOfAboveText;
      var values = this.state.values;
      var classNames = ['form-group', 'form-group--checkbox'];
      if (isDisabled) classNames.push('form-group--disabled');
      if (isRequired) classNames.push('form-group--required');
      var error = errorText;

      if (isRequired && !values.length && !noneOfAbove) {
        error = ERROR_TEXT$1;
      }

      if (error) classNames.push('form-group--error');
      var inputProps = {};
      /* eslint-disable jsx-a11y/label-has-for */

      /* eslint-disable jsx-a11y/label-has-associated-control */

      var checkboxList = list.map(function (it, i) {
        inputProps = _objectSpread2({
          id: "".concat(id, "-").concat(i + 1),
          name: id,
          value: it.value,
          type: 'checkbox',
          checked: values.includes(it.value),
          onChange: _this2.handleOnChange
        }, it.hint && {
          'aria-describedby': "".concat(id, "-hint")
        });
        return React.createElement("div", {
          className: "form-element form-element--checkbox",
          key: "".concat(id, "-").concat(i + 1)
        }, React.createElement("input", inputProps), it.label && React.createElement("label", {
          className: labelClassName,
          htmlFor: "".concat(id, "-").concat(i + 1),
          id: "".concat(id, "-label-").concat(i + 1)
        }, it.label), it.hint && React.createElement("span", {
          id: "".concat(id, "-hint-").concat(i + 1),
          className: "form-element-hint"
        }, it.hint), it.error && React.createElement("span", {
          className: "form-element-error-msg"
        }, it.error));
      });
      return React.createElement("fieldset", {
        className: classNames.join(' ')
      }, labelText && React.createElement("legend", {
        className: "label"
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(id, "-hint"),
        className: "form-group-hint"
      }, hintText), error && React.createElement("span", {
        className: "form-group--error-msg"
      }, error), !!list.length && checkboxList, noneOfAbove && !!list.length && React.createElement("div", {
        className: "form-element form-element--checkbox"
      }, React.createElement("input", {
        id: "".concat(id, "-").concat(list.length + 1),
        name: id,
        type: "checkbox",
        value: noneOfAboveText,
        checked: values.length === 1 && values.includes(noneOfAboveText),
        onChange: this.noneOfAboveChange
      }), React.createElement("label", {
        className: "label",
        htmlFor: "".concat(id, "-").concat(list.length + 1)
      }, React.createElement("strong", null, noneOfAboveText))));
      /* eslint-enable */
    }
  }]);

  return CheckboxGroup;
}(PureComponent);

_defineProperty(CheckboxGroup, "propTypes", {
  id: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    hint: PropTypes.string,
    error: PropTypes.string
  }).isRequired).isRequired,
  values: PropTypes.array,
  labelText: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isDisabled: PropTypes.bool,
  isRequired: PropTypes.bool,
  onChange: PropTypes.func,
  labelClassName: PropTypes.string,
  noneOfAbove: PropTypes.bool,
  noneOfAboveText: PropTypes.string
});

_defineProperty(CheckboxGroup, "defaultProps", {
  values: [],
  onChange: null,
  labelClassName: 'label',
  labelText: '',
  hintText: '',
  errorText: '',
  isDisabled: false,
  isRequired: false,
  noneOfAbove: false,
  noneOfAboveText: 'None of the above'
});

var ERROR_TEXT$2 = 'You have to check at least one item.';

var RadioButtons =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(RadioButtons, _PureComponent);

  /* eslint-disable react/forbid-prop-types */

  /* eslint-enable */
  function RadioButtons(props) {
    var _this;

    _classCallCheck(this, RadioButtons);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(RadioButtons).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleOnChange", function (e) {
      var onChange = _this.props.onChange;
      var value = e.target.value;

      _this.setState({
        value: value
      });

      onChange(value);
    });

    _this.state = {
      value: null
    };
    return _this;
  }

  _createClass(RadioButtons, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var value = this.props.value;
      this.setState({
        value: value
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var value = this.props.value;
      var prevValue = prevProps.value;
      /* eslint-disable react/no-did-update-set-state */

      if (value !== prevValue) {
        this.setState({
          value: value
        });
      }
      /* eslint-enable */

    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var value = this.state.value;
      var _this$props = this.props,
          name = _this$props.name,
          list = _this$props.list,
          labelClassName = _this$props.labelClassName,
          labelText = _this$props.labelText,
          hintText = _this$props.hintText,
          errorText = _this$props.errorText,
          isDisabled = _this$props.isDisabled,
          isRequired = _this$props.isRequired;
      var classNames = ['form-group', 'form-group--radio'];
      if (isDisabled) classNames.push('form-group--disabled');
      if (isRequired) classNames.push('form-group--required');
      var inputProps = {};
      var error = errorText;

      if (isRequired && !value) {
        error = ERROR_TEXT$2;
      }

      if (error) classNames.push('form-group--error');
      /* eslint-disable jsx-a11y/label-has-for */

      /* eslint-disable jsx-a11y/label-has-associated-control */

      var radioButtons = list.map(function (it, i) {
        inputProps = _objectSpread2({
          id: "".concat(name, "-").concat(i + 1),
          name: name,
          value: it.value,
          type: 'radio',
          checked: it.value === value,
          required: isRequired,
          disabled: isDisabled,
          onChange: _this2.handleOnChange
        }, it.hint && {
          'aria-describedby': "".concat(name, "-hint")
        });
        return React.createElement("div", {
          className: "form-element form-element--radio",
          key: "".concat(name, "-").concat(i + 1)
        }, React.createElement("input", inputProps), it.label && React.createElement("label", {
          className: labelClassName,
          htmlFor: "".concat(name, "-").concat(i + 1),
          id: "".concat(it.value, "-label-").concat(i + 1)
        }, it.label), it.hint && React.createElement("span", {
          id: "".concat(it.value, "-hint-").concat(i + 1),
          className: "form-element-hint"
        }, it.hint), it.error && React.createElement("span", {
          className: "form-element-error-msg"
        }, it.error));
      });
      return React.createElement("fieldset", {
        className: classNames.join(' ')
      }, labelText && React.createElement("legend", {
        className: "label"
      }, labelText, isRequired && React.createElement("span", {
        className: "required"
      }, "*")), hintText && React.createElement("span", {
        id: "".concat(name, "-hint"),
        className: "form-group-hint"
      }, hintText), !!list.length && radioButtons);
      /* eslint-enable */
    }
  }]);

  return RadioButtons;
}(PureComponent);

_defineProperty(RadioButtons, "propTypes", {
  name: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    hint: PropTypes.string,
    error: PropTypes.string
  }).isRequired).isRequired,
  value: PropTypes.string,
  labelText: PropTypes.string,
  hintText: PropTypes.string,
  errorText: PropTypes.string,
  isDisabled: PropTypes.bool,
  isRequired: PropTypes.bool,
  onChange: PropTypes.func,
  labelClassName: PropTypes.string
});

_defineProperty(RadioButtons, "defaultProps", {
  value: null,
  onChange: null,
  labelClassName: 'label',
  labelText: '',
  hintText: '',
  errorText: '',
  isDisabled: false,
  isRequired: false
});

export { Button, CheckboxGroup, InputCheckbox, InputEmail, InputField, InputPassword, InputPasswordStrength, InputPin, InputSearch, InputSelect, InputTel, InputText, InputTextarea, Modal, RadioButtons, ReactDatePicker, ReactMultiSelect, ReactSelect, SubmitButton, Toggle };
