/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs';
/* eslint-enable */

addDecorator(withInfo);
addDecorator(withKnobs);
addDecorator(storyFn => <main className="wrapper--center"><section className="content">{storyFn()}</section></main>);

function loadStories() {
  require('../stories/buttons.js');
  require('../stories/inputs.js');
  // You can require as many stories as you need.
}

configure(loadStories, module);
