# Everdapt React Components

This repository contains a growing library of React Components used in various projects

## Components

The library currently contains the following components

### Form fields

- InputEmail
- InputSearch
- InputTel
- InputText
- InputCheckbox
- InputTextarea
- InputPassword
- InputPasswordStrength
- InputPin
- Toggle
- ReactDatePicker
- ReactSelect
- ReactMultiSelect
- CheckboxGroup
- RadioButtons

### Extending

- InputField is a general-purpose input type field designed to be extended and not used directly.

### Buttons
- Button
- SubmitButton

### Modals
- Modal

## Contributing

### Code Style

Everdapt follows [prettier](https://github.com/prettier/prettier) and [airbnb](https://github.com/airbnb/javascript) style guides. A pre-commit hook checks code prior to commit and prevents non standard code from being committed.

On occasion it may be necessary to add an ignore comment to disable a check. This will be reviewed when merging pull requests.

### Testing

- ```yarn test``` will run the full test suite
- ```yarn watch``` will start the test runner in watch mode.

### Changing a component

1. Ensure tests have been written or updated to match new functionality.

2. Update test snapshots if necessary: Run ```yarn test``` and Jest will inform if a new snapshot should be generated.

3. Run the test suite ```yarn test``` prior to committing.

4. Add a commit and patch version for each component that is changed: ```git commit -m "COMPONENT CHANGE MESSAGE"```, ```yarn patch```

5. Submit a pull request

### Adding a new component

#### Pure components
Prefer [pure components](https://reactjs.org/docs/react-api.html#reactpurecomponent) over functional stateless components. React's PureComponent implements ```shouldComponentUpdate()``` which can provide a performance improvement. 

#### Finishing up
When a new component has been created: 

1. Check tests for edge cases and add tests as appropriate.

2. Run the test suite ```yarn test```

3. Import and export the component in ```src/components/index.js``` prior to committing the change.

4. Build the library ```yarn build```

5. Add a new minor version ```yarn minor```

6. Submit a pull request

